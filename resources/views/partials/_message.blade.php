@foreach($errors->all() as $message)
<div class="alert alert-danger alert-dismissible fade show" role="alert">
	  {{ $message }}
</div>
@endforeach

@if(Session::has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
	  {!! Session::get('success') !!}
</div>
@endif
@if(Session::has('danger'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
	  {!! Session::get('danger') !!}
</div>
@endif

@if(Session::has('warning'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
	  {!! Session::get('warning') !!}
</div>
@endif