<!doctype html>
<html lang='en' dir='ltr' id='content_html'>
<head>
<meta charset='utf-8'>
<meta http-equiv='content-language' content='en'>
<title>Send international mobile recharge. Recharge mobiles online, easy & safe.</title>
<meta http-equiv='X-UA-Compatible' content='IE=edge, chrome=1'>

<meta name='viewport' content='initial-scale=1.0, width=device-width, user-scalable=no'>
<meta http-equiv='imagetoolbar' content='no'>
<link rel='stylesheet' href='{{ url('css/style.css') }}' type='text/css' media='all'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet'
	type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300' rel='stylesheet' type='text/css'>

<link href='https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet'>

<script src='//code.jquery.com/jquery-3.3.1.min.js' integrity='sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=' crossorigin='anonymous'></script>
<script src='//code.jquery.com/jquery-migrate-3.0.1.min.js' integrity='sha256-F0O1TmEa4I8N24nY0bya59eP6svWcshqX1uzwaWC4F4=' crossorigin='anonymous'></script>

<script src='{{ url('js/script.js') }}'></script>
<meta name="csrf-token" content="{{ csrf_token() }}" />
@yield('style')
<!--[if lt IE 9]>
<script src='//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js'></script>
<script src='/js/respond.src.js'></script>
<link rel='stylesheet' href='/css/lt_ie9.css' type='text/css' media='screen'>
<![endif]-->
</head>
<body class='homepage'>
<noscript>
		<div class='noscript_message'>
		In order to view all our website features you need to <a href="http://www.enable-javascript.com/en" target="_blank" rel="noreferrer noopener">enable Javascript.</a>		<img src='/images/js_disabled_tracking.gif?cid=509d2e1f64b7a80f3b0e834204eaae58' alt='' width='1' height='1'>
	</div>
</noscript>
<svg width="0" height="0" style="position:absolute;"><symbol id="svg_facebook" ><path d="M15.117 0H.883C.395 0 0 .395 0 .883v14.234c0 .488.395.883.883.883h7.663V9.804H6.46V7.39h2.086V5.607c0-2.066 1.262-3.19 3.106-3.19.883 0 1.642.064 1.863.094v2.16h-1.28c-1 0-1.195.48-1.195 1.18v1.54h2.39l-.31 2.42h-2.08V16h4.077c.488 0 .883-.395.883-.883V.883C16 .395 15.605 0 15.117 0"/></symbol><symbol id="svg_instagram" ><path d="M8 0C5.827 0 5.555.01 4.702.048 3.85.088 3.27.222 2.76.42c-.526.204-.973.478-1.417.923-.445.444-.72.89-.923 1.417-.198.51-.333 1.09-.372 1.942C.008 5.555 0 5.827 0 8s.01 2.445.048 3.298c.04.852.174 1.433.372 1.942.204.526.478.973.923 1.417.444.445.89.72 1.417.923.51.198 1.09.333 1.942.372.853.04 1.125.048 3.298.048s2.445-.01 3.298-.048c.852-.04 1.433-.174 1.942-.372.526-.204.973-.478 1.417-.923.445-.444.72-.89.923-1.417.198-.51.333-1.09.372-1.942.04-.853.048-1.125.048-3.298s-.01-2.445-.048-3.298c-.04-.852-.174-1.433-.372-1.942-.204-.526-.478-.973-.923-1.417-.444-.445-.89-.72-1.417-.923-.51-.198-1.09-.333-1.942-.372C10.445.008 10.173 0 8 0zm0 1.44c2.136 0 2.39.01 3.233.048.78.036 1.203.166 1.485.276.374.145.64.318.92.598.28.28.453.546.598.92.11.282.24.705.276 1.485.038.844.047 1.097.047 3.233s-.01 2.39-.05 3.233c-.04.78-.17 1.203-.28 1.485-.15.374-.32.64-.6.92-.28.28-.55.453-.92.598-.28.11-.71.24-1.49.276-.85.038-1.1.047-3.24.047s-2.39-.01-3.24-.05c-.78-.04-1.21-.17-1.49-.28-.38-.15-.64-.32-.92-.6-.28-.28-.46-.55-.6-.92-.11-.28-.24-.71-.28-1.49-.03-.84-.04-1.1-.04-3.23s.01-2.39.04-3.24c.04-.78.17-1.21.28-1.49.14-.38.32-.64.6-.92.28-.28.54-.46.92-.6.28-.11.7-.24 1.48-.28.85-.03 1.1-.04 3.24-.04zm0 2.452c-2.27 0-4.108 1.84-4.108 4.108 0 2.27 1.84 4.108 4.108 4.108 2.27 0 4.108-1.84 4.108-4.108 0-2.27-1.84-4.108-4.108-4.108zm0 6.775c-1.473 0-2.667-1.194-2.667-2.667 0-1.473 1.194-2.667 2.667-2.667 1.473 0 2.667 1.194 2.667 2.667 0 1.473-1.194 2.667-2.667 2.667zm5.23-6.937c0 .53-.43.96-.96.96s-.96-.43-.96-.96.43-.96.96-.96.96.43.96.96z"/></symbol><symbol id="svg_twitter" ><path d="M16 3.038c-.59.26-1.22.437-1.885.517.677-.407 1.198-1.05 1.443-1.816-.634.37-1.337.64-2.085.79-.598-.64-1.45-1.04-2.396-1.04-1.812 0-3.282 1.47-3.282 3.28 0 .26.03.51.085.75-2.728-.13-5.147-1.44-6.766-3.42C.83 2.58.67 3.14.67 3.75c0 1.14.58 2.143 1.46 2.732-.538-.017-1.045-.165-1.487-.41v.04c0 1.59 1.13 2.918 2.633 3.22-.276.074-.566.114-.865.114-.21 0-.41-.02-.61-.058.42 1.304 1.63 2.253 3.07 2.28-1.12.88-2.54 1.404-4.07 1.404-.26 0-.52-.015-.78-.045 1.46.93 3.18 1.474 5.04 1.474 6.04 0 9.34-5 9.34-9.33 0-.14 0-.28-.01-.42.64-.46 1.2-1.04 1.64-1.7z" fill-rule="nonzero"/></symbol><symbol id="svg_linkedin" ><path d="M13.632 13.635h-2.37V9.922c0-.886-.018-2.025-1.234-2.025-1.235 0-1.424.964-1.424 1.96v3.778h-2.37V6H8.51v1.04h.03c.318-.6 1.092-1.233 2.247-1.233 2.4 0 2.845 1.58 2.845 3.637v4.188zM3.558 4.955c-.762 0-1.376-.617-1.376-1.377 0-.758.614-1.375 1.376-1.375.76 0 1.376.617 1.376 1.375 0 .76-.617 1.377-1.376 1.377zm1.188 8.68H2.37V6h2.376v7.635zM14.816 0H1.18C.528 0 0 .516 0 1.153v13.694C0 15.484.528 16 1.18 16h13.635c.652 0 1.185-.516 1.185-1.153V1.153C16 .516 15.467 0 14.815 0z" fill-rule="nonzero"/></symbol><symbol id="svg_youtube" ><path d="M0 7.345c0-1.294.16-2.59.16-2.59s.156-1.1.636-1.587c.608-.637 1.408-.617 1.764-.684C3.84 2.36 8 2.324 8 2.324s3.362.004 5.6.166c.314.038.996.04 1.604.678.48.486.636 1.588.636 1.588S16 6.05 16 7.346v1.258c0 1.296-.16 2.59-.16 2.59s-.156 1.102-.636 1.588c-.608.638-1.29.64-1.604.678-2.238.162-5.6.166-5.6.166s-4.16-.037-5.44-.16c-.356-.067-1.156-.047-1.764-.684-.48-.487-.636-1.587-.636-1.587S0 9.9 0 8.605v-1.26zm6.348 2.73V5.58l4.323 2.255-4.32 2.24z"/></symbol><symbol id="svg_googleplus" ><path d="M5.09 7.273v1.745h2.89c-.116.75-.873 2.197-2.887 2.197-1.737 0-3.155-1.44-3.155-3.215S3.353 4.785 5.09 4.785c.99 0 1.652.422 2.03.786l1.382-1.33c-.887-.83-2.037-1.33-3.41-1.33C2.275 2.91 0 5.19 0 8s2.276 5.09 5.09 5.09c2.94 0 4.888-2.065 4.888-4.974 0-.334-.036-.59-.08-.843H5.09zm10.91 0h-1.455V5.818H13.09v1.455h-1.454v1.454h1.455v1.455h1.46V8.727H16"/></symbol><symbol id="svg_icon_checkmark" viewBox="0 0 24 24">
		<path class="checkmark_stroke" fill="currentColor" d="M 12,1 C 17.514,1 23,6.486 23,12 23,17.514 17.514,23 12,23 6.486,23 1,17.514 1,12 1,6.486 6.486,1 12,1 Z M 12,0 C 5.373,0 0,5.373 0,12 0,18.627 5.373,24 12,24 18.627,24 24,18.627 24,12 24,5.373 18.627,0 12,0 Z M 16.393,7.5 10.75,13.284 8.106,10.778 6.25,12.636 10.75,17 18.25,9.357 Z"/>
		<path class="checkmark_check" fill="currentColor" d="m 8.5243646,14.823879 c -1.2215527,-1.184535 -2.2245622,-2.164371 -2.22891,-2.177414 -0.00435,-0.01304 0.4023883,-0.433896 0.9038578,-0.935229 l 0.911763,-0.911513 1.3212581,1.253369 1.3212585,1.253369 0.309278,-0.323021 c 0.170103,-0.177662 1.439381,-1.480819 2.820618,-2.895905 l 2.511341,-2.5728832 0.921598,0.9209783 0.921598,0.9209783 -0.478508,0.4826492 c -0.26318,0.2654574 -1.651217,1.6795564 -3.084528,3.1424434 -1.43331,1.462886 -2.903829,2.960412 -3.267819,3.327835 l -0.6618,0.668041 z"/>
		<path class="checkmark_bg" d="M 11.08828,22.938451 C 9.9632602,22.815894 8.8499136,22.481173 7.7334593,21.929844 6.4978473,21.319672 5.4603214,20.5696 4.4440236,19.551771 3.4021448,18.508323 2.6406931,17.446688 2.029817,16.185824 1.4671845,15.024537 1.1393142,13.865881 1.0404994,12.68969 1.0033729,12.247774 1.0227467,11.350806 1.0785486,10.928092 1.3926703,8.5485417 2.614236,6.2340586 4.5587112,4.334278 6.2059952,2.7248576 8.1551255,1.6404036 10.181853,1.2056834 c 2.853906,-0.61214481 5.852325,0.2542683 8.538749,2.4673283 0.411803,0.3392415 1.237126,1.1622978 1.580173,1.5758343 1.694866,2.0431282 2.60616,4.2521941 2.679653,6.495741 0.05466,1.668478 -0.406121,3.419237 -1.334924,5.072165 -1.188485,2.115066 -3.059231,3.922454 -5.201191,5.025038 -1.187345,0.611192 -2.355446,0.97146 -3.559874,1.097943 -0.442912,0.04651 -1.363459,0.04585 -1.796159,-0.0013 z m 3.451261,-9.783554 C 16.346527,11.310979 17.92342,9.7019812 18.043749,9.5793456 L 18.262528,9.3563723 17.326941,8.4210284 16.391354,7.4856849 13.575974,10.370942 c -1.548459,1.586892 -2.824073,2.881949 -2.834697,2.877905 -0.01062,-0.004 -0.600768,-0.558487 -1.3114319,-1.232095 -0.7106637,-0.673608 -1.3047842,-1.227729 -1.3202672,-1.23138 -0.04404,-0.01039 -1.886536,1.845 -1.8583986,1.871398 0.013234,0.01242 1.0311748,0.998478 2.2620905,2.19125 l 2.2380282,2.168676 0.251407,-0.254611 c 0.138274,-0.140036 1.72985,-1.763271 3.536836,-3.607188 z"/>
	</symbol><defs>

		<linearGradient id="linear-gradient" x1="21.8" y1="173.29" x2="5.02" y2="156.51" gradientTransform="matrix(1, 0, 0, -1, 0, 182)" gradientUnits="userSpaceOnUse">
			<stop offset="0" stop-color="#00a0ff"/>
			<stop offset="0.01" stop-color="#00a1ff"/>
			<stop offset="0.26" stop-color="#00beff"/>
			<stop offset="0.51" stop-color="#00d2ff"/>
			<stop offset="0.76" stop-color="#00dfff"/>
			<stop offset="1" stop-color="#00e3ff"/>
		</linearGradient>
		<linearGradient id="linear-gradient-2" x1="33.83" y1="162" x2="9.64" y2="162" gradientTransform="matrix(1, 0, 0, -1, 0, 182)" gradientUnits="userSpaceOnUse">
			<stop offset="0" stop-color="#ffe000"/>
			<stop offset="0.41" stop-color="#ffbd00"/>
			<stop offset="0.78" stop-color="orange"/>
			<stop offset="1" stop-color="#ff9c00"/>
		</linearGradient>
		<linearGradient id="linear-gradient-3" x1="24.83" y1="159.7" x2="2.07" y2="136.95" gradientTransform="matrix(1, 0, 0, -1, 0, 182)" gradientUnits="userSpaceOnUse">
			<stop offset="0" stop-color="#ff3a44"/>
			<stop offset="1" stop-color="#c31162"/>
		</linearGradient>
		<linearGradient id="linear-gradient-4" x1="7.3" y1="181.82" x2="17.46" y2="171.66" gradientTransform="matrix(1, 0, 0, -1, 0, 182)" gradientUnits="userSpaceOnUse">
			<stop offset="0" stop-color="#32a071"/>
			<stop offset="0.07" stop-color="#2da771"/>
			<stop offset="0.48" stop-color="#15cf74"/>
			<stop offset="0.8" stop-color="#06e775"/>
			<stop offset="1" stop-color="#00f076"/>
		</linearGradient>
	</defs><linearGradient id="linear-gradient" x1="21.8" y1="173.29" x2="5.02" y2="156.51" gradientTransform="matrix(1, 0, 0, -1, 0, 182)" gradientUnits="userSpaceOnUse">
			<stop offset="0" stop-color="#00a0ff"/>
			<stop offset="0.01" stop-color="#00a1ff"/>
			<stop offset="0.26" stop-color="#00beff"/>
			<stop offset="0.51" stop-color="#00d2ff"/>
			<stop offset="0.76" stop-color="#00dfff"/>
			<stop offset="1" stop-color="#00e3ff"/>
		</linearGradient><linearGradient id="linear-gradient-2" x1="33.83" y1="162" x2="9.64" y2="162" gradientTransform="matrix(1, 0, 0, -1, 0, 182)" gradientUnits="userSpaceOnUse">
			<stop offset="0" stop-color="#ffe000"/>
			<stop offset="0.41" stop-color="#ffbd00"/>
			<stop offset="0.78" stop-color="orange"/>
			<stop offset="1" stop-color="#ff9c00"/>
		</linearGradient><linearGradient id="linear-gradient-3" x1="24.83" y1="159.7" x2="2.07" y2="136.95" gradientTransform="matrix(1, 0, 0, -1, 0, 182)" gradientUnits="userSpaceOnUse">
			<stop offset="0" stop-color="#ff3a44"/>
			<stop offset="1" stop-color="#c31162"/>
		</linearGradient><linearGradient id="linear-gradient-4" x1="7.3" y1="181.82" x2="17.46" y2="171.66" gradientTransform="matrix(1, 0, 0, -1, 0, 182)" gradientUnits="userSpaceOnUse">
			<stop offset="0" stop-color="#32a071"/>
			<stop offset="0.07" stop-color="#2da771"/>
			<stop offset="0.48" stop-color="#15cf74"/>
			<stop offset="0.8" stop-color="#06e775"/>
			<stop offset="1" stop-color="#00f076"/>
		</linearGradient><symbol id="svg_stroke">
		<path d="M130.2,40H4.73A4.74,4.74,0,0,1,0,35.27V4.73A4.73,4.73,0,0,1,4.73,0H130.2A4.8,4.8,0,0,1,135,4.73V35.27A4.8,4.8,0,0,1,130.2,40Z"/>
	</symbol><symbol id="svg_holder">
		<path d="M134,35.27a3.83,3.83,0,0,1-3.83,3.83H4.73A3.83,3.83,0,0,1,.89,35.27V4.72A3.84,3.84,0,0,1,4.73.89H130.2A3.83,3.83,0,0,1,134,4.72V35.27Z"/>
	</symbol><symbol id="svg_AppleStore" ><g id="AppleStore">
		<use xlink:href="#stroke"/>
		<use xlink:href="#holder"/>

		<g class="logomark">
			<path d="M30.13,19.78a5.8,5.8,0,0,1,2.76-4.86,5.94,5.94,0,0,0-4.68-2.53c-2-.21-3.87,1.18-4.88,1.18s-2.57-1.16-4.23-1.12a6.23,6.23,0,0,0-5.24,3.2c-2.27,3.92-.58,9.69,1.6,12.86,1.09,1.55,2.36,3.29,4,3.23s2.23-1,4.19-1,2.51,1,4.21,1,2.84-1.56,3.89-3.13a12.83,12.83,0,0,0,1.78-3.62A5.6,5.6,0,0,1,30.13,19.78Z"/>
			<path d="M26.93,10.31a5.71,5.71,0,0,0,1.31-4.09,5.81,5.81,0,0,0-3.76,1.94,5.43,5.43,0,0,0-1.34,3.94A4.8,4.8,0,0,0,26.93,10.31Z"/>
		</g>
		<g class="logotext">
			<g id="AppStore">
				<path d="M53.65,31.5H51.37l-1.24-3.91H45.81L44.62,31.5H42.41L46.69,18.2h2.65ZM49.76,26l-1.12-3.48Q48.45,21.95,48,20h0q-.2.85-.63,2.51L46.18,26Z"/>
				<path d="M64.66,26.59a5.45,5.45,0,0,1-1.32,3.87,3.87,3.87,0,0,1-2.94,1.26,2.94,2.94,0,0,1-2.72-1.36h0v5.05H55.5V25.07q0-1.54-.08-3.16H57.3l.12,1.52h0a3.79,3.79,0,0,1,6.07-.38A5.29,5.29,0,0,1,64.66,26.59Zm-2.17.08a4,4,0,0,0-.63-2.31A2.18,2.18,0,0,0,60,23.41a2.22,2.22,0,0,0-1.43.52,2.43,2.43,0,0,0-.84,1.37,2.79,2.79,0,0,0-.1.65v1.6a2.56,2.56,0,0,0,.64,1.77,2.13,2.13,0,0,0,1.67.72,2.18,2.18,0,0,0,1.88-.93A4.08,4.08,0,0,0,62.49,26.67Z"/>
				<path d="M75.7,26.59a5.45,5.45,0,0,1-1.32,3.87,3.86,3.86,0,0,1-2.94,1.26,2.94,2.94,0,0,1-2.72-1.36h0v5.05H66.54V25.07q0-1.54-.08-3.16h1.88l.12,1.52h0a3.79,3.79,0,0,1,6.07-.38A5.3,5.3,0,0,1,75.7,26.59Zm-2.17.08a4,4,0,0,0-.63-2.31A2.18,2.18,0,0,0,71,23.41a2.22,2.22,0,0,0-1.43.52,2.42,2.42,0,0,0-.84,1.37,2.85,2.85,0,0,0-.1.65v1.6a2.57,2.57,0,0,0,.64,1.77A2.13,2.13,0,0,0,71,30a2.18,2.18,0,0,0,1.88-.93A4.08,4.08,0,0,0,73.53,26.67Z"/>
				<path d="M88,27.77a3.55,3.55,0,0,1-1.18,2.76,5.24,5.24,0,0,1-3.62,1.17,6.35,6.35,0,0,1-3.45-.83l.49-1.78a5.84,5.84,0,0,0,3.08.85,2.91,2.91,0,0,0,1.88-.54,1.78,1.78,0,0,0,.67-1.45,1.85,1.85,0,0,0-.55-1.36,5.14,5.14,0,0,0-1.84-1Q80,24.25,80,21.74A3.38,3.38,0,0,1,81.25,19a4.83,4.83,0,0,1,3.26-1,6.46,6.46,0,0,1,3,.63L87,20.37a5.24,5.24,0,0,0-2.55-.61,2.58,2.58,0,0,0-1.76.55,1.58,1.58,0,0,0-.53,1.2,1.63,1.63,0,0,0,.61,1.3,6.91,6.91,0,0,0,1.94,1,6.57,6.57,0,0,1,2.53,1.62A3.38,3.38,0,0,1,88,27.77Z"/>
				<path d="M95.09,23.51H92.74v4.66q0,1.78,1.24,1.78a3.79,3.79,0,0,0,.95-.1L95,31.46a4.83,4.83,0,0,1-1.66.24,2.56,2.56,0,0,1-2-.77,3.78,3.78,0,0,1-.71-2.59V23.51h-1.4v-1.6h1.4V20.15l2.09-.63v2.39h2.35Z"/>
				<path d="M105.69,26.63a5.26,5.26,0,0,1-1.26,3.63,4.51,4.51,0,0,1-3.52,1.46,4.29,4.29,0,0,1-3.36-1.4,5.1,5.1,0,0,1-1.25-3.53,5.21,5.21,0,0,1,1.29-3.65,4.47,4.47,0,0,1,3.48-1.42,4.38,4.38,0,0,1,3.4,1.4A5,5,0,0,1,105.69,26.63Zm-2.21.07a4.32,4.32,0,0,0-.57-2.28A2.11,2.11,0,0,0,101,23.27a2.14,2.14,0,0,0-2,1.15,4.4,4.4,0,0,0-.57,2.32A4.31,4.31,0,0,0,99,29a2.18,2.18,0,0,0,3.85,0A4.33,4.33,0,0,0,103.48,26.7Z"/>
				<path d="M112.62,23.78a3.7,3.7,0,0,0-.67-.06,2,2,0,0,0-1.74.85,3.19,3.19,0,0,0-.53,1.9v5h-2.13l0-6.57q0-1.66-.08-3h1.86l.08,1.84h.06a3.28,3.28,0,0,1,1.07-1.52,2.58,2.58,0,0,1,1.54-.51,3.42,3.42,0,0,1,.53,0Z"/>
				<path d="M122.16,26.25a5,5,0,0,1-.08,1h-6.4a2.78,2.78,0,0,0,.93,2.17,3.17,3.17,0,0,0,2.09.67,7.07,7.07,0,0,0,2.59-.45l.33,1.48a8,8,0,0,1-3.22.59,4.66,4.66,0,0,1-3.51-1.31,4.85,4.85,0,0,1-1.27-3.52,5.5,5.5,0,0,1,1.19-3.61,4.09,4.09,0,0,1,3.36-1.54,3.58,3.58,0,0,1,3.14,1.54A5.19,5.19,0,0,1,122.16,26.25Zm-2-.55a2.9,2.9,0,0,0-.41-1.64,1.86,1.86,0,0,0-1.7-.89,2,2,0,0,0-1.7.87,3.18,3.18,0,0,0-.63,1.66h4.44Z"/>
			</g>
			<g id="Download_on_the">
				<path d="M49,10A3.28,3.28,0,0,1,48,12.67a4.21,4.21,0,0,1-2.78.82,12.9,12.9,0,0,1-1.53-.08V7a11.31,11.31,0,0,1,1.81-.14,3.89,3.89,0,0,1,2.59.75A3,3,0,0,1,49,10Zm-1.1,0a2.39,2.39,0,0,0-.61-1.76,2.39,2.39,0,0,0-1.77-.61,4.44,4.44,0,0,0-.84.07v4.89a5.53,5.53,0,0,0,.71,0A2.48,2.48,0,0,0,47.29,12,2.68,2.68,0,0,0,47.94,10Z"/>
				<path d="M54.91,11a2.59,2.59,0,0,1-.62,1.78,2.22,2.22,0,0,1-1.73.72,2.11,2.11,0,0,1-1.65-.69,2.51,2.51,0,0,1-.62-1.74,2.56,2.56,0,0,1,.63-1.79,2.2,2.2,0,0,1,1.71-.7,2.16,2.16,0,0,1,1.67.69A2.49,2.49,0,0,1,54.91,11Zm-1.09,0A2.13,2.13,0,0,0,53.54,10a1,1,0,0,0-.94-.56,1.05,1.05,0,0,0-1,.56,2.16,2.16,0,0,0-.28,1.14,2.13,2.13,0,0,0,.28,1.12,1.06,1.06,0,0,0,1,.56,1,1,0,0,0,.94-.57A2.12,2.12,0,0,0,53.82,11.07Z"/>
				<path d="M62.76,8.72l-1.47,4.71h-1l-.61-2a15.32,15.32,0,0,1-.38-1.52h0a11.15,11.15,0,0,1-.38,1.52l-.65,2h-1L55.93,8.72H57L57.54,11q.19.79.32,1.51h0q.12-.59.39-1.5l.67-2.25h.85l.64,2.2q.23.81.38,1.55h0a14.83,14.83,0,0,1,.32-1.55l.57-2.2h1Z"/>
				<path d="M68.2,13.43h-1v-2.7q0-1.25-.95-1.25a1,1,0,0,0-.76.34,1.22,1.22,0,0,0-.29.81v2.8h-1V10.07q0-.62,0-1.35H65l0,.74h0a1.51,1.51,0,0,1,.54-.57,1.77,1.77,0,0,1,.95-.27,1.5,1.5,0,0,1,1.1.43,2.07,2.07,0,0,1,.54,1.56Z"/>
				<path d="M71.09,13.43H70V6.56h1Z"/>
				<path d="M77.26,11a2.59,2.59,0,0,1-.62,1.78,2.22,2.22,0,0,1-1.73.72,2.1,2.1,0,0,1-1.65-.69,2.51,2.51,0,0,1-.61-1.74,2.56,2.56,0,0,1,.64-1.79A2.2,2.2,0,0,1,75,8.62a2.16,2.16,0,0,1,1.67.69A2.49,2.49,0,0,1,77.26,11Zm-1.09,0A2.13,2.13,0,0,0,75.89,10a1,1,0,0,0-.94-.56A1,1,0,0,0,74,10a2.16,2.16,0,0,0-.28,1.14A2.13,2.13,0,0,0,74,12.21a1.06,1.06,0,0,0,1,.56,1,1,0,0,0,.94-.57A2.11,2.11,0,0,0,76.17,11.07Z"/>
				<path d="M82.33,13.43h-.94l-.08-.54h0a1.61,1.61,0,0,1-1.38.65,1.42,1.42,0,0,1-1.08-.43,1.34,1.34,0,0,1-.37-1,1.46,1.46,0,0,1,.72-1.32,3.75,3.75,0,0,1,2-.45V10.3q0-.93-1-.93A2.19,2.19,0,0,0,79,9.72L78.8,9a3,3,0,0,1,1.62-.41q1.85,0,1.85,1.95v1.74A7.36,7.36,0,0,0,82.33,13.43Zm-1.09-1.62v-.73q-1.73,0-1.73.95a.71.71,0,0,0,.2.55.73.73,0,0,0,.51.18,1.06,1.06,0,0,0,.64-.22.89.89,0,0,0,.38-.74Z"/>
				<path d="M88.29,13.43h-.93l0-.76h0a1.57,1.57,0,0,1-1.51.86,1.77,1.77,0,0,1-1.42-.67,2.61,2.61,0,0,1-.56-1.74,2.73,2.73,0,0,1,.61-1.85,1.88,1.88,0,0,1,1.46-.66,1.41,1.41,0,0,1,1.33.64h0V6.56h1v5.61Q88.25,12.85,88.29,13.43Zm-1.09-2v-.79a1.54,1.54,0,0,0,0-.33,1.19,1.19,0,0,0-.38-.64,1,1,0,0,0-.7-.26,1.08,1.08,0,0,0-.92.47,2,2,0,0,0-.34,1.19,1.86,1.86,0,0,0,.32,1.14,1.08,1.08,0,0,0,.92.47,1,1,0,0,0,.83-.39A1.29,1.29,0,0,0,87.2,11.44Z"/>
				<path d="M97.25,11a2.59,2.59,0,0,1-.62,1.78,2.21,2.21,0,0,1-1.73.72,2.11,2.11,0,0,1-1.65-.69,2.51,2.51,0,0,1-.61-1.74,2.56,2.56,0,0,1,.64-1.79A2.2,2.2,0,0,1,95,8.62a2.15,2.15,0,0,1,1.67.69A2.49,2.49,0,0,1,97.25,11Zm-1.09,0A2.13,2.13,0,0,0,95.88,10a1,1,0,0,0-.94-.56A1.05,1.05,0,0,0,94,10a2.16,2.16,0,0,0-.28,1.14A2.13,2.13,0,0,0,94,12.21a1.07,1.07,0,0,0,1.89,0A2.13,2.13,0,0,0,96.16,11.07Z"/>
				<path d="M102.88,13.43h-1v-2.7q0-1.25-1-1.25a.94.94,0,0,0-.76.34,1.21,1.21,0,0,0-.29.81v2.8h-1V10.07q0-.62,0-1.35h.92l0,.74h0a1.53,1.53,0,0,1,.54-.57,1.77,1.77,0,0,1,1-.27,1.5,1.5,0,0,1,1.1.43,2.07,2.07,0,0,1,.54,1.56Z"/>
				<path d="M109.94,9.5h-1.15v2.29c0,.58.21.87.61.87a1.85,1.85,0,0,0,.47,0l0,.8a2.35,2.35,0,0,1-.81.12,1.25,1.25,0,0,1-1-.38,1.85,1.85,0,0,1-.35-1.27V9.5h-.69V8.72h.69V7.86l1-.31V8.72h1.15V9.5Z"/>
				<path d="M115.48,13.43h-1V10.75q0-1.27-.95-1.27a1,1,0,0,0-1,.73,1.32,1.32,0,0,0,0,.38v2.84h-1V6.56h1V9.4h0a1.58,1.58,0,0,1,1.42-.77A1.46,1.46,0,0,1,115,9a2.15,2.15,0,0,1,.53,1.58Z"/>
				<path d="M121.21,10.85a2.53,2.53,0,0,1,0,.48H118a1.37,1.37,0,0,0,.46,1.07,1.56,1.56,0,0,0,1,.33,3.47,3.47,0,0,0,1.27-.22l.16.73a4,4,0,0,1-1.58.29,2.28,2.28,0,0,1-1.72-.64,2.38,2.38,0,0,1-.62-1.73,2.7,2.7,0,0,1,.58-1.77,2,2,0,0,1,1.65-.76,1.75,1.75,0,0,1,1.54.76A2.52,2.52,0,0,1,121.21,10.85Zm-1-.27a1.41,1.41,0,0,0-.2-.81.92.92,0,0,0-.83-.44,1,1,0,0,0-.83.43,1.59,1.59,0,0,0-.31.81Z"/>
			</g>
		</g>
	</g></symbol><symbol id="svg_GooglePlay" ><g id="GooglePlay">
		<use xlink:href="#stroke"/>
		<use xlink:href="#holder"/>
		<g class="logotext">
			<g id="Get_it_on">
				<path d="M47.42,10.24a2.71,2.71,0,0,1-.75,2,2.91,2.91,0,0,1-2.2.89A3.09,3.09,0,0,1,41.35,10a3.09,3.09,0,0,1,3.12-3.13,3.1,3.1,0,0,1,1.23.25,2.47,2.47,0,0,1,.94.67l-.53.53a2,2,0,0,0-1.64-.71A2.32,2.32,0,0,0,42.14,10a2.36,2.36,0,0,0,4,1.73,1.89,1.89,0,0,0,.5-1.22H44.47V9.79h2.91A2.54,2.54,0,0,1,47.42,10.24Z"/>
				<path d="M52,7.74H49.3v1.9h2.46v.72H49.3v1.9H52V13h-3.5V7H52Z"/>
				<path d="M55.28,13h-.77V7.74H52.83V7H57v.74H55.28Z"/>
				<path d="M59.94,13V7h.77v6Z"/>
				<path d="M64.13,13h-.77V7.74H61.68V7H65.8v.74H64.13Z"/>
				<path d="M73.61,12.22a3.12,3.12,0,0,1-4.4,0A3.07,3.07,0,0,1,68.33,10a3.07,3.07,0,0,1,.88-2.22,3.1,3.1,0,0,1,4.4,0A3.07,3.07,0,0,1,74.49,10,3.07,3.07,0,0,1,73.61,12.22Zm-3.83-.5a2.31,2.31,0,0,0,3.26,0A2.35,2.35,0,0,0,73.71,10,2.35,2.35,0,0,0,73,8.28a2.31,2.31,0,0,0-3.26,0A2.35,2.35,0,0,0,69.11,10,2.35,2.35,0,0,0,69.78,11.72Z"/>
				<path d="M75.58,13V7h.94l2.92,4.67h0l0-1.16V7h.77v6h-.8L76.35,8.11h0l0,1.16V13Z"/>
			</g>
			<g id="GooglePlayStore">
				<path d="M68.14,21.75A4.25,4.25,0,1,0,72.41,26,4.19,4.19,0,0,0,68.14,21.75Zm0,6.83A2.58,2.58,0,1,1,70.54,26,2.46,2.46,0,0,1,68.14,28.58Zm-9.31-6.83A4.25,4.25,0,1,0,63.09,26,4.19,4.19,0,0,0,58.82,21.75Zm0,6.83A2.58,2.58,0,1,1,61.22,26,2.46,2.46,0,0,1,58.82,28.58ZM47.74,23.06v1.8h4.32a3.77,3.77,0,0,1-1,2.27,4.42,4.42,0,0,1-3.33,1.32,4.8,4.8,0,0,1,0-9.6A4.6,4.6,0,0,1,51,20.14l1.27-1.27A6.29,6.29,0,0,0,47.74,17a6.61,6.61,0,1,0,0,13.21,6,6,0,0,0,4.61-1.85,6,6,0,0,0,1.56-4.22,5.87,5.87,0,0,0-.1-1.13Zm45.31,1.4a4,4,0,0,0-3.64-2.71,4,4,0,0,0-4,4.25,4.16,4.16,0,0,0,4.22,4.25,4.23,4.23,0,0,0,3.54-1.88l-1.45-1a2.43,2.43,0,0,1-2.09,1.18,2.16,2.16,0,0,1-2.06-1.29l5.69-2.35Zm-5.8,1.42a2.33,2.33,0,0,1,2.22-2.48,1.65,1.65,0,0,1,1.58.9ZM82.63,30H84.5V17.5H82.63Zm-3.06-7.3H79.5a3,3,0,0,0-2.24-1,4.26,4.26,0,0,0,0,8.51,2.9,2.9,0,0,0,2.24-1h.06v.61c0,1.63-.87,2.5-2.27,2.5a2.35,2.35,0,0,1-2.14-1.51l-1.63.68a4.05,4.05,0,0,0,3.77,2.51c2.19,0,4-1.29,4-4.43V22H79.57Zm-2.14,5.88a2.59,2.59,0,0,1,0-5.16A2.4,2.4,0,0,1,79.7,26,2.38,2.38,0,0,1,77.42,28.58ZM101.81,17.5H97.33V30H99.2V25.26h2.61a3.89,3.89,0,1,0,0-7.76Zm0,6H99.2V19.24h2.65a2.14,2.14,0,1,1,0,4.29Zm11.53-1.8a3.5,3.5,0,0,0-3.33,1.91l1.66.69a1.77,1.77,0,0,1,1.7-.92,1.8,1.8,0,0,1,2,1.61v.13a4.13,4.13,0,0,0-1.95-.48c-1.79,0-3.6,1-3.6,2.81a2.89,2.89,0,0,0,3.1,2.75A2.63,2.63,0,0,0,115.32,29h.06v1h1.8V25.19C117.18,23,115.52,21.73,113.39,21.73Zm-.23,6.85c-.61,0-1.46-.31-1.46-1.06,0-1,1.06-1.33,2-1.33a3.32,3.32,0,0,1,1.7.42A2.26,2.26,0,0,1,113.16,28.58ZM123.74,22l-2.14,5.42h-.06L119.32,22h-2l3.33,7.58-1.9,4.21h1.95L125.82,22Zm-16.81,8h1.87V17.5h-1.87Z"/>
			</g>
		</g>
		<g class="logomark">
			<path class="cls-4" d="M10.44,7.54A2,2,0,0,0,10,8.94V31.06a2,2,0,0,0,.46,1.4l.07.07L22.9,20.15v-.29L10.51,7.47Z"/>
			<path class="cls-5" d="M27,24.28,22.9,20.15v-.29L27,15.72l.09.05L32,18.56c1.4.79,1.4,2.09,0,2.89l-4.89,2.78Z"/>
			<path class="cls-6" d="M27.12,24.22,22.9,20,10.44,32.46a1.63,1.63,0,0,0,2.08.06l14.61-8.3"/>
			<path class="cls-7" d="M27.12,15.78,12.51,7.48a1.63,1.63,0,0,0-2.08.06L22.9,20Z"/>
			<g>
				<path class="cls-8" d="M27,24.13,12.51,32.38a1.67,1.67,0,0,1-2,0h0l-.07.07h0l.07.07h0a1.66,1.66,0,0,0,2,0l14.61-8.3Z"/>
				<path class="cls-9" d="M10.44,32.32a2,2,0,0,1-.46-1.4v.15a2,2,0,0,0,.46,1.4l.07-.07Z"/>
			</g>
			<path class="cls-9" d="M32,21.3l-5,2.83.09.09L32,21.44a1.75,1.75,0,0,0,1-1.44h0A1.86,1.86,0,0,1,32,21.3Z"/>
			<path class="top-Highlight" d="M12.51,7.62,32,18.7a1.86,1.86,0,0,1,1,1.3h0a1.75,1.75,0,0,0-1-1.44L12.51,7.48C11.12,6.68,10,7.34,10,8.94v.15C10,7.49,11.12,6.83,12.51,7.62Z"/>
		</g>
	</g></symbol><symbol id="svg_AnyPhone" ><g id="AnyPhone">
		<use xlink:href="#stroke"/>
		<use xlink:href="#holder"/>

		<g class="logotext">
			<path d="M43.61,25.88l-.94-2.61H38.25l-.9,2.61H34.82l5.13-13.05h1l5.18,13.05Zm-3.16-9.11L38.9,21.52H42Z"/>
			<path d="M53.41,25.88V20.44A2.73,2.73,0,0,0,53,18.68a2.37,2.37,0,0,0-3.4.4v6.8h-2.2V16.46h1.58l.4.88c1.48-1.75,6.27-1.7,6.27,2.76v5.77Z"/>
			<path d="M62.15,27.39c-.39,1-1.82,2.18-4.2,2.18V27.63q2.54,0,2.54-1.27c0-1.08-.56-2.23-3.65-9.89h2.28L61.68,23,64,16.46h2.28Z"/>
			<path d="M75.43,21.14v4.74H73.15V13c3.66-.15,8.54-.53,8.54,3.76C81.69,20.37,79,21.48,75.43,21.14Zm0-6.15v4.12c3.22.34,3.92-.8,3.92-2.16s-1.08-2-3.23-2A5.32,5.32,0,0,0,75.43,15Z"/>
			<path d="M89.67,25.88v-5.8a2,2,0,0,0-3.07-1.63,2,2,0,0,0-.79.67v6.76H83.58V13l2.23-.53v4.63c1.43-1.3,6.06-1.42,6.06,3v5.8Z"/>
			<path d="M98.16,16.29c3,0,4.52,1.94,4.52,4.86a4.57,4.57,0,0,1-7.85,3.56C92.6,22.22,93.3,16.29,98.16,16.29Zm-2.24,4.86q0,3.11,2.24,3.11c1.59,0,2.23-1.44,2.23-3.11q0-3.07-2.23-3.07C96.59,18.08,95.92,19.49,95.92,21.15Z"/>
			<path d="M110.49,25.88V20.44a2.73,2.73,0,0,0-.46-1.76,2.37,2.37,0,0,0-3.4.4v6.8h-2.2V16.46H106l.4.88c1.48-1.75,6.27-1.7,6.27,2.76v5.77Z"/>
			<path d="M123.46,21.84h-6.72c.16,3,3.81,2.76,5,1.59l.85,1.68a5.41,5.41,0,0,1-3.46.94c-6.63,0-5.86-9.76,0-9.76C122.12,16.29,124.22,18.5,123.46,21.84Zm-6.64-1.65h4.62a2.06,2.06,0,0,0-2.29-2.07A2.26,2.26,0,0,0,116.82,20.19Z"/>
			<path d="M18.91,27.05a1,1,0,0,0-1.41-.13c-.25.21-.51.42-.69.54-.37.32-.31.14-.44,0-.56-1.61-3.41-10.31-3.2-13.42a1,1,0,0,1,1.26-.85,1,1,0,0,0,1.23-.69l1.57-5.68a1,1,0,0,0-.7-1.23l-1.8-.49c-4.3-1.41-5.16,7.13-3.48,15.44,1.46,7.23,6.25,17.23,10,13.74l1.4-1.12a1,1,0,0,0,.15-1.41ZM24.32,12.5H19a.93.93,0,0,0-.93.93V23a.93.93,0,0,0,.93.93h5.31a.93.93,0,0,0,.93-.93V13.43A.93.93,0,0,0,24.32,12.5ZM20.46,22.69H19.25v-.6h1.21Zm0-1.2H19.25v-.6h1.21Zm0-1.2H19.25v-.6h1.21Zm0-1.2H19.25v-.6h1.21Zm1.8,3.6H21.05v-.6h1.21Zm0-1.2H21.05v-.6h1.21Zm0-1.2H21.05v-.6h1.21Zm0-1.2H21.05v-.6h1.21Zm1.8,3.6H22.85v-.6h1.21Zm0-1.2H22.85v-.6h1.21Zm0-1.2H22.85v-.6h1.21Zm0-1.2H22.85v-.6h1.21Zm0-2.1a.31.31,0,0,1-.31.31h-4.2a.31.31,0,0,1-.31-.31V14a.31.31,0,0,1,.31-.31h4.2a.31.31,0,0,1,.31.31Z"/>
		</g>
	</g></symbol><symbol id="svg_WebCall" ><g id="WebCall">
		<use xlink:href="#stroke"/>
		<use xlink:href="#holder"/>

		<g class="logotext">
			<path d="M32.39,22.41C32.49,14.47,27.54,8,21.42,8,15.7,8,11,13.65,10.49,20.87a7.64,7.64,0,0,0,6.1,10.31c.84,1.31,4.12.94,4.12-.44s-3.14-1.75-4.08-.51A6.49,6.49,0,0,1,11.39,26a2.85,2.85,0,0,0,2.39,1.3c1.58,0,1.91-1.29,1.91-2.88V20.13c0-1.59-.33-2.88-1.91-2.88a2.83,2.83,0,0,0-1.87.72c1.35-5.11,5.09-8.78,9.51-8.78s8.15,3.67,9.51,8.78a2.83,2.83,0,0,0-1.87-.72c-1.58,0-1.91,1.29-1.91,2.88v4.32c0,1.59.33,2.88,1.91,2.88a2.87,2.87,0,0,0,2.86-2.88v-2Z"/>
			<path d="M57.49,26.79h-.94l-2.81-8.12L51,26.79h-.94L45.83,13.74h2.38l2.42,7.77,2.61-7.77h1l2.62,7.77,2.41-7.77h2.38Z"/>
			<path d="M66.72,17A4.28,4.28,0,0,1,71,22.58H64.24c.16,2.93,3.79,2.77,5,1.59l.85,1.68a5.41,5.41,0,0,1-3.46.94C60,26.79,60.7,17,66.72,17Zm-2.41,3.93h4.62a2.06,2.06,0,0,0-2.29-2.07A2.26,2.26,0,0,0,64.32,20.93Z"/>
			<path d="M74.46,26.1l-.49.69H72.73V13.69l2.2-.53v4.39a4.41,4.41,0,0,1,6.51,4.11C81.44,27,76.49,27.59,74.46,26.1Zm.47-6.65v4.91a1.63,1.63,0,0,0,1.31.57c2,0,2.92-.82,2.92-3.17q0-2.87-2.74-2.87A1.92,1.92,0,0,0,74.93,19.44Z"/>
			<path d="M97.75,14.42l-.94,1.89a3.37,3.37,0,0,0-2.46-.76c-4.69,0-5.07,9.25-.17,9.25A3.43,3.43,0,0,0,97,23.5l1.06,1.85A5.36,5.36,0,0,1,94,26.83c-3.88,0-5.7-3-5.7-6.43s1.91-6.88,5.92-6.88A6.19,6.19,0,0,1,97.75,14.42Z"/>
			<path d="M105,25.67a3.22,3.22,0,0,1-2.59,1.12c-3.9,0-4-4-2-5.48a5.89,5.89,0,0,1,4.4-.79q0-1.66-2.1-1.66a4.66,4.66,0,0,0-2.07.41l-.47-1.71c1.6-.77,4.55-.83,5.85.43,2,1.93.11,7,1.76,8.09-.29.5-.58.69-1.37.69A1.56,1.56,0,0,1,105,25.67Zm-.21-3.49c-1.52-.31-3.45,0-3.45,1.59q0,1.27,1.47,1.27C105,25,104.83,23.4,104.83,22.18Z"/>
			<path d="M109.77,13.69l2.2-.53V23.75q0,1.74,1,2.07a1.81,1.81,0,0,1-1.74,1q-1.49,0-1.49-2.07Z"/>
			<path d="M115.08,13.69l2.2-.53V23.75q0,1.74,1,2.07a1.81,1.81,0,0,1-1.74,1q-1.49,0-1.49-2.07Z"/>
		</g>
	</g></symbol><defs>
        <style>
            .stars-0 {
            fill: url(#holder--0-Stars);
            }

            .stars-1 {
            fill: url(#holder--1-Stars);
            }

            .stars-2 {
            fill: url(#holder--2-Stars);
            }

            .stars-3 {
            fill: url(#holder--3-Stars);
            }

            .stars-4 {
            fill: url(#holder--4-Stars);
            }

            .stars-5 {
            fill: url(#holder--5-Stars);
            }

            .star-color {
            fill: #ffffff;
			}
        </style>

        <linearGradient id="holder--0-Stars" x1="50" y1="98" x2="50" y2="2" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#E5E5E5"/>
            <stop offset="1" stop-color="#E5E5E5"/>
        </linearGradient>
        <linearGradient id="holder--1-Stars" x1="50" y1="98" x2="50" y2="2" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#FF3722"/>
            <stop offset="1" stop-color="#FF3722"/>
        </linearGradient>
        <linearGradient id="holder--2-Stars" x1="50" y1="98" x2="50" y2="2" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#FF8622"/>
            <stop offset="1" stop-color="#FF8622"/>
        </linearGradient>
        <linearGradient id="holder--3-Stars" x1="50" y1="98" x2="50" y2="2" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#FFCE00"/>
            <stop offset="1" stop-color="#FFCE00"/>
        </linearGradient>
        <linearGradient id="holder--4-Stars" x1="50" y1="98" x2="50" y2="2" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#73CF11"/>
            <stop offset="1" stop-color="#73CF11"/>
        </linearGradient>
        <linearGradient id="holder--5-Stars" x1="50" y1="98" x2="50" y2="2" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#00B67A"/>
            <stop offset="1" stop-color="#00B67A"/>
        </linearGradient>
    </defs><linearGradient id="holder--0-Stars" x1="50" y1="98" x2="50" y2="2" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#E5E5E5"/>
            <stop offset="1" stop-color="#E5E5E5"/>
        </linearGradient><linearGradient id="holder--1-Stars" x1="50" y1="98" x2="50" y2="2" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#FF3722"/>
            <stop offset="1" stop-color="#FF3722"/>
        </linearGradient><linearGradient id="holder--2-Stars" x1="50" y1="98" x2="50" y2="2" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#FF8622"/>
            <stop offset="1" stop-color="#FF8622"/>
        </linearGradient><linearGradient id="holder--3-Stars" x1="50" y1="98" x2="50" y2="2" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#FFCE00"/>
            <stop offset="1" stop-color="#FFCE00"/>
        </linearGradient><linearGradient id="holder--4-Stars" x1="50" y1="98" x2="50" y2="2" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#73CF11"/>
            <stop offset="1" stop-color="#73CF11"/>
        </linearGradient><linearGradient id="holder--5-Stars" x1="50" y1="98" x2="50" y2="2" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#00B67A"/>
            <stop offset="1" stop-color="#00B67A"/>
        </linearGradient><symbol id="svg_star-rating" viewBox="0 0 100 100">
        <rect class="star-rating-gradient" x="2" y="2" width="96" height="96" rx="0" ry="0"/>
        <path class="star-color" d="M50,66.7L64.6,63l6.1,18.8L50,66.7z M83.6,42.4H57.9L50,18.2l-7.9,24.2H16.4l20.8,15l-7.9,24.2l20.8-15             l12.8-9.2L83.6,42.4L83.6,42.4L83.6,42.4L83.6,42.4z"/>
    </symbol><symbol id="svg_icon-circle-checkmark">
    <circle id="holder-circle-checkmark" class="color-responsive" cx="32" cy="32" r="32"/>
    <path id="checkmark" d="M46.146.146a.5.5 0 0 1 .708.708L14.5 33.207.146 18.854a.5.5 0 0 1 .708-.708L14.5 31.793 46.146.146z"/>
  </symbol><symbol id="svg_trustpilot-logo">
        <g id="trustpilot-logotype">
            <path class="text" d="M294.4,71.2v15.2h-32.1V172h-17.6V86.4h-31.9V71.2C227.9,71.2,279.3,71.2,294.4,71.2z M306,99v14.1h0.3                 c1.3-5,6.1-11.3,14.1-14.7c5.1-2,8.3-1.4,13.3-1V113c-17.9-3.4-26.7,7.8-26.7,24.3V172h-16.1V99C296.2,99,300.1,99,306,99z                  M407.6,172h-15.8v-10.2h-0.3c-4.1,7.6-13,12.2-20.8,12.2c-18,0-27.2-8.1-27.2-28.7V99h16.1v44.8c0,10.7,3.5,17.6,14,17.6                 c12.3,0,17.9-7.5,17.9-19.9V99.1h16.1V172z M434.9,148.6c0.5,4.7,2.3,8,5.3,9.9c3.1,1.9,6.8,2.8,11.2,2.8c1.5,0,3.2-0.1,5.1-0.4                 s3.8-0.7,5.4-1.4c1.7-0.6,3.1-1.6,4.2-2.9c1.1-1.3,1.6-2.9,1.5-5c-0.1-2.1-0.9-3.8-2.3-5.1c-1.4-1.4-3.2-2.3-5.4-3.2                 c-2.2-0.8-4.7-1.5-7.5-2.1c-2.8-0.6-5.7-1.2-8.6-1.9c-3-0.6-5.9-1.5-8.7-2.4c-2.8-0.9-5.3-2.2-7.5-3.8c-2.2-1.6-4-3.6-5.3-6.1                 c-1.4-2.5-2-5.6-2-9.3c0-4,1-7.3,2.9-10c1.9-2.7,4.4-4.8,7.3-6.5c3-1.6,6.3-2.8,9.9-3.5c3.6-0.6,7.1-1,10.4-1                 c3.8,0,7.4,0.4,10.8,1.2c3.4,0.8,6.5,2.1,9.3,3.9c2.8,1.8,5.1,4.1,6.9,7c1.8,2.8,3,6.3,3.5,10.4h-16.8c-0.8-3.8-2.5-6.5-5.3-7.8                 c-2.8-1.4-6-2-9.5-2c-1.1,0-2.5,0.1-4.1,0.3c-1.6,0.2-3,0.6-4.4,1.1c-1.4,0.5-2.5,1.3-3.5,2.3c-0.9,1-1.4,2.3-1.4,3.9                 c0,2,0.7,3.6,2.1,4.8c1.4,1.2,3.1,2.2,5.3,3.1c2.2,0.8,4.7,1.5,7.5,2.1c2.8,0.6,5.8,1.2,8.8,1.9c2.9,0.6,5.8,1.5,8.6,2.4                 s5.3,2.2,7.5,3.8c2.2,1.6,4,3.6,5.3,6c1.4,2.4,2.1,5.5,2.1,9c0,4.3-1,8-3,11c-2,3-4.6,5.5-7.7,7.3c-3.1,1.9-6.7,3.3-10.5,4.1                 c-3.8,0.9-7.7,1.3-11.5,1.3c-4.6,0-8.9-0.5-12.8-1.6c-3.9-1.1-7.3-2.6-10.2-4.7c-2.8-2.1-5.1-4.8-6.8-7.9                 c-1.6-3.1-2.5-6.9-2.6-11.2h16.2V148.6z M488,99h12.2V77.1h16.1V99h14.5v12h-14.5c0.1,32.8-1.4,44.1,2.2,47.7                 c2.5,2.5,10.8,1.3,12.5,0.9V172c-37.7,4.5-30.1-8.1-30.6-60.9h-12.2V99H488z M542.2,99h15.2v9.9h0.3                 c10.6-19.9,56.9-17.8,56.8,27.1c-0.1,43.5-43,45.9-55.9,26.5h-0.3V199h-16.1V99z M598.4,135.6c0-32.7-40.6-37-40.6,0                 C557.7,169.7,598.4,170.5,598.4,135.6z M626.7,71.2h16.1v15.2h-16.1V71.2z M626.7,99h16.1v73h-16.1V99z M657.2,71.2h16.1V172                 h-16.1V71.2z M688.2,151c-4.6-13.1-3.4-31.5,7.2-43.1c17.9-19.6,64-14.6,64,27.5C759.4,182,700.5,186.1,688.2,151z M722.6,161.3                 c24.2,0,27.4-39,9.3-49.3c-5.3-3-13.2-3.1-18.6,0C695.2,122.3,698.4,161.3,722.6,161.3z M764.1,99h12.2V77.1h16.1V99h14.5v12                 h-14.5c0.1,32.8-1.4,44.1,2.2,47.7c2.5,2.5,10.8,1.3,12.5,0.9V172c-12.8,1.5-26.8,2.2-29.7-9.8c-1.7-8-0.6-11.5-0.9-51.1h-12.2                 V99H764.1z"/>
            </g>

        <g id="trustpilot-logomark">
            <polygon class="star-full" points="194,71.2 120.3,71.2 97.5,1 74.7,71.2 1,71.1 60.7,114.5 37.8,184.6 97.5,141.3 157.1,184.6                 134.4,114.5 194,71.2 194,71.2"/>
            <polygon class="star-cutout" points="139.5,130.4 134.4,114.5 97.5,141.3"/>
        </g>
    </symbol></svg><header class='padding_vertical10'>
	<nav class='navbar navbar-inverse'>
		<div class='container-fluid'>
			<div class='row-fluid'>
				<div class='span12'>
					<div class='nav_menu_trigger_section'>
						<button data-target='.nav-collapse' data-toggle='collapse' class='btn btn-navbar' type='button' aria-label='Menu'>
							<span class='icon-bar'></span>
							<span class='icon-bar'></span>
							<span class='icon-bar'></span>
						</button>
					</div>
					<div class="logo_section" style="min-height:80px;padding-top: 10px;">
						<a href='/' class='logo_holder' style="text-decoration: none;font-size:20px;font-weight: 900;color:white">{{ $data['logo'] }}</a>
					</div>
					<div class='nav_menu_section'>
						<ul class='nav navbar-nav nav-collapse collapse clearfix'>
							<div class='top_menu'>
								<div class='row-fluid'>
									<div class='top_menu_inner'>
											
<li class='dropdown'  id="dropdownLogin">
	@if(!Auth::check())
		<a role="button" class='dropdown-toggle login_button btn btn-primary color_white' data-toggle='dropdown'>
			Log in<b class='caret'></b>
		</a>
	@else
		<a role="button" class='dropdown-toggle login_button btn btn-primary color_white' data-toggle='dropdown'>
			Welcome <strong>{{ Auth::user()->fname }}</strong><b class='caret'></b>
		</a>
	@endif
	@if(!Auth::Check())
	<div class='log_in_menu dropdown-menu padding0' id="loginPopup">
		<div class='padding_top20 padding_horizontal20 padding_bottom0'>
			<form id='menu_login' class='form-inline' method="POST" action="{{ route('login') }}" id="account_login_form">
				@csrf
				<input name='email' type='email' placeholder='Email' tabindex='1'>
				<div class='input-append'>
					<input class='width65' name='password' type='password' placeholder='Password' tabindex='2'><input class='btn btn-primary width35' type='submit' tabindex='3' value='Log in &raquo;'>
				</div>
			</form>
		</div>
		<div class='margin_top10 padding20 border_top bg_cross_pattern hidden-phone'>
			<div class='pagination-centered hidden-phone'>
				<a class='btn btn-primary' href='{{route('register')}}' tabindex='4'>Create new account &raquo;</a>
			</div>
		</div>
	</div>
	@else
		<ul class="dropdown-menu" id="loginPopup">
			@if(in_array('vendor', explode(',', Auth::user()->role)))
			<li>
				<a href="{{route('vendorHome')}}" class="{{Request::is('vendor/home') ? 'active' : null}}">Home</a>
			</li>
			@elseif(in_array('user', explode(',', Auth::user()->role)))
			<li>
				<a href="{{route('userHome')}}" class="{{Request::is('user/home') ? 'active' : null}}" style="padding: 5px auto">Home</a>
			</li>
            @endif
			<li>
				<a href="{{ route('logout') }}"
					onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">
					{{ __('Logout') }}
				</a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					@csrf
				</form>
			</li>
		</ul>
	@endif
</li>
								</div>
							</div>
						</ul>
					</div>

				</div>
			</div>
		</div>
	</nav>
</header>
@yield('content')

<script nonce='cHBuTTVNMDdFcWNXNnJEVjc1bWI='>

if (!window.messages) messages = {};
messages["MANDATORY_FIELD"] = "This field is mandatory";
messages["ERROR_EMAIL_ADDRESS_ALREADY_IN_USE"] = "Email address is already in use.";
messages["CONTACT_NO_MSG_ERR"] = "Please enter a valid email address.";
messages["ERROR_VALID_URL"] = "Please enter a valid URL.";
messages["ERROR_VALID_DATE"] = "Please enter a valid date.";
messages["ERROR_VALID_NUMBER"] = "Please enter a valid number.";
messages["ERROR_ENTER_DIGITS_ONLY"] = "Please enter digits only.";
messages["INVALID_CARD_NUMBER"] = "Invalid credit card number";
messages["ERROR_ENTER_EMAIL_ADDRESS_AGAIN"] = "Please enter the email address again";
messages["NO_RESULTS_FOUND"] = "No results found.";
messages["SELECT"] = "Select";
messages["FIRST_MONTH_THEN_RECURRING"] = "==first_sell== for 1<sup>st<\/sup> month<br>then ==recurring_sell==";
messages["SEARCH_COUNTRY"] = "Enter your location";
//-->
</script><script>
	// $('#dropdownLogin').click(() => {
	// 	$('#dropdownLogin').toggleClass('open');
	// })
	// $('#loginPopup').click(() => {
	// 	$('#dropdownLogin').toggleClass('open');
	// })

	var show_cookie_notice = false;
	$(window).on('load', function () {
		if (show_cookie_notice) {
			var cookie_notice_message = $('<span/>', {
				'html': messages.cookie_notice,
				'class': 'cookie_notice_message'
			});
			var cookie_notice_close = $('<div/>', {
				'html': '&times;',
				'class': 'cookie_notice_close'
			});

			var cookie_notice_holder = $('<div/>', {'id': 'cookie_notice_holder'}).append('<div/>');
			cookie_notice_holder.find('div').append(cookie_notice_message).append(cookie_notice_close);
			$('body').append(cookie_notice_holder);

			$('#cookie_notice_holder .cookie_notice_close').click(function () {
				var d = new Date();
				d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
				var expires = 'expires=' + d.toGMTString();
				document.cookie = 'cookie_notice_accepted=1; ' + expires;
				$('#cookie_notice_holder').remove();
			});
		}
	});

	var __currency = {"id":"1","name":"United States Dollars","code":"USD","symbol":"$","minor_symbol":"\u00a2","exponent":"2","rate":"1.0000000000"};
	var __rate_precision = "1";
	var global_message = null;
	var global_v3_error_message = null;
	var global_v3_notice_message = null;
	var global_v3_warning_message = null;
	var global_v3_success_message = null;

	$.extend(jQuery.validator.messages, {
		required: messages.MANDATORY_FIELD,
		remote: messages.ERROR_EMAIL_ADDRESS_ALREADY_IN_USE,
		email: messages.CONTACT_NO_MSG_ERR,
		url: messages.ERROR_VALID_URL,
		date: messages.ERROR_VALID_DATE,
		number: messages.ERROR_VALID_NUMBER,
		phone: messages.ERROR_VALID_NUMBER,
		digits: messages.ERROR_ENTER_DIGITS_ONLY,
		creditcard: messages.INVALID_CARD_NUMBER,
		equalTo: messages.ERROR_ENTER_EMAIL_ADDRESS_AGAIN
	});
	$(function () {
		if (global_message) {
			if ((_page_name != 'subscribe') || !('success' in global_message))
				show_page_message(global_message);
		}

		if (global_v3_success_message) {
			if (_page_name != 'subscribe')
				show_page_message(global_v3_success_message, 'status_success');
		}

		if (global_v3_error_message)
			show_page_message(global_v3_error_message, 'status_error');

		if (global_v3_notice_message)
			show_page_message(global_v3_notice_message, 'status_notice');

		if (global_v3_warning_message)
			show_page_message(global_v3_warning_message, 'status_warning');

		if (is_Android) {
			$('.footer_mobile_ios').addClass('none');
			try {
				Android.closeWindow();
			} catch (error) {}
			;
		}
		if (is_iOS) {
			$('.footer_mobile_android').addClass('none');
		}
		if (is_iOSwebview) {
			window.location.hash = "iosClosewindow";
		}
	});
	$('.slider_text').click(function () {
		var text_width = $('.slider_text').width();
		var zone_countries = [{"id":"40","name":"Abkhazia","country_codes":"ab","currency_id":"2","default_language_id":null,"priority":"40","status":"active"},{"id":"41","name":"Afghanistan","country_codes":"af","currency_id":"1","default_language_id":null,"priority":"41","status":"active"},{"id":"42","name":"\u00c5land Islands","country_codes":"ax","currency_id":"2","default_language_id":null,"priority":"42","status":"active"},{"id":"43","name":"Albania","country_codes":"al","currency_id":"2","default_language_id":null,"priority":"43","status":"active"},{"id":"44","name":"Algeria","country_codes":"dz","currency_id":"1","default_language_id":null,"priority":"44","status":"active"},{"id":"45","name":"American Samoa","country_codes":"as","currency_id":"1","default_language_id":null,"priority":"45","status":"active"},{"id":"46","name":"Andorra","country_codes":"ad","currency_id":"2","default_language_id":null,"priority":"46","status":"active"},{"id":"47","name":"Angola","country_codes":"ao","currency_id":"1","default_language_id":null,"priority":"47","status":"active"},{"id":"48","name":"Anguilla","country_codes":"ai","currency_id":"1","default_language_id":null,"priority":"48","status":"active"},{"id":"49","name":"Antarctica","country_codes":"aq","currency_id":"1","default_language_id":null,"priority":"49","status":"active"},{"id":"50","name":"Antigua and Barbuda","country_codes":"ag","currency_id":"1","default_language_id":null,"priority":"50","status":"active"},{"id":"51","name":"Argentina","country_codes":"ar","currency_id":"1","default_language_id":null,"priority":"51","status":"active"},{"id":"52","name":"Armenia","country_codes":"am","currency_id":"1","default_language_id":null,"priority":"52","status":"active"},{"id":"53","name":"Aruba","country_codes":"aw","currency_id":"1","default_language_id":null,"priority":"53","status":"active"},{"id":"54","name":"Ascension Island","country_codes":"ac","currency_id":"1","default_language_id":null,"priority":"54","status":"active"},{"id":"16","name":"Australia","country_codes":"au","currency_id":"6","default_language_id":"1","priority":"2","status":"active"},{"id":"34","name":"Austria","country_codes":"at","currency_id":"2","default_language_id":"11","priority":"34","status":"active"},{"id":"55","name":"Azerbaijan","country_codes":"az","currency_id":"1","default_language_id":null,"priority":"55","status":"active"},{"id":"56","name":"Bahamas","country_codes":"bs","currency_id":"1","default_language_id":null,"priority":"56","status":"active"},{"id":"57","name":"Bahrain","country_codes":"bh","currency_id":"1","default_language_id":null,"priority":"57","status":"active"},{"id":"58","name":"Bangladesh","country_codes":"bd","currency_id":"1","default_language_id":null,"priority":"58","status":"active"},{"id":"59","name":"Barbados","country_codes":"bb","currency_id":"1","default_language_id":null,"priority":"59","status":"active"},{"id":"60","name":"Belarus","country_codes":"by","currency_id":"2","default_language_id":null,"priority":"60","status":"active"},{"id":"39","name":"Belgium","country_codes":"be","currency_id":"2","default_language_id":null,"priority":"21","status":"active"},{"id":"61","name":"Belize","country_codes":"bz","currency_id":"1","default_language_id":null,"priority":"61","status":"active"},{"id":"62","name":"Benin","country_codes":"bj","currency_id":"1","default_language_id":null,"priority":"62","status":"active"},{"id":"63","name":"Bermuda","country_codes":"bm","currency_id":"1","default_language_id":null,"priority":"63","status":"active"},{"id":"64","name":"Bhutan","country_codes":"bt","currency_id":"1","default_language_id":null,"priority":"64","status":"active"},{"id":"65","name":"Bolivia","country_codes":"bo","currency_id":"1","default_language_id":null,"priority":"65","status":"active"},{"id":"66","name":"Bonaire, Saint Eustatius and Saba","country_codes":"bq","currency_id":"1","default_language_id":null,"priority":"66","status":"active"},{"id":"67","name":"Bosnia and Herzegovina","country_codes":"ba","currency_id":"2","default_language_id":null,"priority":"67","status":"active"},{"id":"68","name":"Botswana","country_codes":"bw","currency_id":"1","default_language_id":null,"priority":"68","status":"active"},{"id":"69","name":"Bouvet Island","country_codes":"bv","currency_id":"2","default_language_id":null,"priority":"69","status":"active"},{"id":"37","name":"Brazil","country_codes":"br","currency_id":"38","default_language_id":"5","priority":"37","status":"active"},{"id":"70","name":"British Indian Ocean Trty.","country_codes":"io","currency_id":"1","default_language_id":null,"priority":"70","status":"active"},{"id":"71","name":"British Virgin Islands","country_codes":"vg","currency_id":"1","default_language_id":null,"priority":"71","status":"active"},{"id":"72","name":"Brunei","country_codes":"bn","currency_id":"1","default_language_id":null,"priority":"72","status":"active"},{"id":"73","name":"Bulgaria","country_codes":"bg","currency_id":"2","default_language_id":null,"priority":"73","status":"active"},{"id":"74","name":"Burkina Faso","country_codes":"bf","currency_id":"1","default_language_id":null,"priority":"74","status":"active"},{"id":"75","name":"Burundi","country_codes":"bi","currency_id":"1","default_language_id":null,"priority":"75","status":"active"},{"id":"76","name":"Cambodia","country_codes":"kh","currency_id":"1","default_language_id":null,"priority":"76","status":"active"},{"id":"77","name":"Cameroon","country_codes":"cm","currency_id":"1","default_language_id":null,"priority":"77","status":"active"},{"id":"10","name":"Canada","country_codes":"ca","currency_id":"5","default_language_id":"1","priority":"4","status":"active"},{"id":"78","name":"Cape Verde","country_codes":"cv","currency_id":"1","default_language_id":null,"priority":"78","status":"active"},{"id":"79","name":"Cayman Islands","country_codes":"ky","currency_id":"1","default_language_id":null,"priority":"79","status":"active"},{"id":"80","name":"Central African Republic","country_codes":"cf","currency_id":"1","default_language_id":null,"priority":"80","status":"active"},{"id":"81","name":"Chad","country_codes":"td","currency_id":"1","default_language_id":null,"priority":"81","status":"active"},{"id":"24","name":"Chile","country_codes":"cl","currency_id":"3","default_language_id":"1","priority":"24","status":"active"},{"id":"82","name":"China","country_codes":"cn","currency_id":"1","default_language_id":null,"priority":"82","status":"active"},{"id":"83","name":"Christmas Island","country_codes":"cx","currency_id":"6","default_language_id":null,"priority":"83","status":"active"},{"id":"84","name":"Cocos Islands","country_codes":"cc","currency_id":"1","default_language_id":null,"priority":"84","status":"active"},{"id":"85","name":"Colombia","country_codes":"co","currency_id":"1","default_language_id":null,"priority":"85","status":"active"},{"id":"86","name":"Comoros","country_codes":"km","currency_id":"2","default_language_id":null,"priority":"86","status":"active"},{"id":"87","name":"Congo","country_codes":"cg","currency_id":"1","default_language_id":null,"priority":"87","status":"active"},{"id":"88","name":"Cook Islands","country_codes":"ck","currency_id":"1","default_language_id":null,"priority":"88","status":"active"},{"id":"89","name":"Costa Rica","country_codes":"cr","currency_id":"1","default_language_id":null,"priority":"89","status":"active"},{"id":"90","name":"Croatia","country_codes":"hr","currency_id":"2","default_language_id":null,"priority":"90","status":"active"},{"id":"91","name":"Cuba","country_codes":"cu","currency_id":"1","default_language_id":null,"priority":"91","status":"active"},{"id":"92","name":"Curacao","country_codes":"cw","currency_id":"1","default_language_id":null,"priority":"92","status":"active"},{"id":"93","name":"Cyprus","country_codes":"cy","currency_id":"2","default_language_id":null,"priority":"93","status":"active"},{"id":"94","name":"Czech Republic","country_codes":"cz","currency_id":"2","default_language_id":null,"priority":"94","status":"active"},{"id":"95","name":"Democratic Republic Of Congo","country_codes":"cd","currency_id":"1","default_language_id":null,"priority":"95","status":"active"},{"id":"96","name":"Denmark","country_codes":"dk","currency_id":"2","default_language_id":null,"priority":"96","status":"active"},{"id":"97","name":"Diego Garcia","country_codes":"dg","currency_id":"1","default_language_id":null,"priority":"97","status":"active"},{"id":"98","name":"Djibouti","country_codes":"dj","currency_id":"1","default_language_id":null,"priority":"98","status":"active"},{"id":"99","name":"Dominica","country_codes":"dm","currency_id":"1","default_language_id":null,"priority":"99","status":"active"},{"id":"100","name":"Dominican Republic","country_codes":"do","currency_id":"1","default_language_id":null,"priority":"100","status":"active"},{"id":"245","name":"East Timor","country_codes":"tl","currency_id":"1","default_language_id":null,"priority":"245","status":"active"},{"id":"101","name":"Ecuador","country_codes":"ec","currency_id":"1","default_language_id":null,"priority":"101","status":"active"},{"id":"102","name":"Egypt","country_codes":"eg","currency_id":"1","default_language_id":null,"priority":"102","status":"active"},{"id":"103","name":"El Salvador","country_codes":"sv","currency_id":"1","default_language_id":null,"priority":"103","status":"active"},{"id":"104","name":"Equatorial Guinea","country_codes":"gq","currency_id":"1","default_language_id":null,"priority":"104","status":"active"},{"id":"105","name":"Eritrea","country_codes":"er","currency_id":"1","default_language_id":null,"priority":"105","status":"active"},{"id":"106","name":"Estonia","country_codes":"ee","currency_id":"2","default_language_id":null,"priority":"106","status":"active"},{"id":"107","name":"Ethiopia","country_codes":"et","currency_id":"1","default_language_id":null,"priority":"107","status":"active"},{"id":"108","name":"Falkland Islands","country_codes":"fk","currency_id":"1","default_language_id":null,"priority":"108","status":"active"},{"id":"109","name":"Faroe Islands","country_codes":"fo","currency_id":"2","default_language_id":null,"priority":"109","status":"active"},{"id":"110","name":"Fiji","country_codes":"fj","currency_id":"1","default_language_id":null,"priority":"110","status":"active"},{"id":"111","name":"Finland","country_codes":"fi","currency_id":"2","default_language_id":null,"priority":"111","status":"active"},{"id":"12","name":"France","country_codes":"fr","currency_id":"2","default_language_id":"4","priority":"6","status":"active"},{"id":"112","name":"French Guiana","country_codes":"gf","currency_id":"2","default_language_id":null,"priority":"112","status":"active"},{"id":"113","name":"French Polynesia","country_codes":"pf","currency_id":"2","default_language_id":null,"priority":"113","status":"active"},{"id":"114","name":"French Southern Territories","country_codes":"tf","currency_id":"2","default_language_id":null,"priority":"114","status":"active"},{"id":"115","name":"Gabon","country_codes":"ga","currency_id":"1","default_language_id":null,"priority":"115","status":"active"},{"id":"116","name":"Gambia","country_codes":"gm","currency_id":"1","default_language_id":null,"priority":"116","status":"active"},{"id":"117","name":"Georgia","country_codes":"ge","currency_id":"1","default_language_id":null,"priority":"117","status":"active"},{"id":"13","name":"Germany","country_codes":"de","currency_id":"2","default_language_id":"11","priority":"8","status":"active"},{"id":"118","name":"Ghana","country_codes":"gh","currency_id":"1","default_language_id":null,"priority":"118","status":"active"},{"id":"119","name":"Gibraltar","country_codes":"gi","currency_id":"2","default_language_id":null,"priority":"119","status":"active"},{"id":"36","name":"Greece","country_codes":"gr","currency_id":"2","default_language_id":null,"priority":"19","status":"active"},{"id":"120","name":"Greenland","country_codes":"gl","currency_id":"1","default_language_id":null,"priority":"120","status":"active"},{"id":"121","name":"Grenada","country_codes":"gd","currency_id":"1","default_language_id":null,"priority":"121","status":"active"},{"id":"122","name":"Guadeloupe","country_codes":"gp","currency_id":"2","default_language_id":null,"priority":"122","status":"active"},{"id":"123","name":"Guam","country_codes":"gu","currency_id":"1","default_language_id":null,"priority":"123","status":"active"},{"id":"124","name":"Guatemala","country_codes":"gt","currency_id":"1","default_language_id":null,"priority":"124","status":"active"},{"id":"125","name":"Guernsey","country_codes":"gg","currency_id":"3","default_language_id":null,"priority":"125","status":"active"},{"id":"126","name":"Guinea","country_codes":"gn","currency_id":"1","default_language_id":null,"priority":"126","status":"active"},{"id":"127","name":"Guinea-Bissau","country_codes":"gw","currency_id":"1","default_language_id":null,"priority":"127","status":"active"},{"id":"128","name":"Guyana","country_codes":"gy","currency_id":"1","default_language_id":null,"priority":"128","status":"active"},{"id":"129","name":"Haiti","country_codes":"ht","currency_id":"1","default_language_id":null,"priority":"129","status":"active"},{"id":"130","name":"Heard Island and McDonald Islands","country_codes":"hm","currency_id":"6","default_language_id":null,"priority":"130","status":"active"},{"id":"131","name":"Honduras","country_codes":"hn","currency_id":"1","default_language_id":null,"priority":"131","status":"active"},{"id":"132","name":"Hong Kong","country_codes":"hk","currency_id":"1","default_language_id":null,"priority":"132","status":"active"},{"id":"133","name":"Hungary","country_codes":"hu","currency_id":"2","default_language_id":null,"priority":"133","status":"active"},{"id":"134","name":"Iceland","country_codes":"is","currency_id":"2","default_language_id":null,"priority":"134","status":"active"},{"id":"135","name":"India","country_codes":"in","currency_id":"1","default_language_id":null,"priority":"135","status":"active"},{"id":"136","name":"Indonesia","country_codes":"id","currency_id":"1","default_language_id":null,"priority":"136","status":"active"},{"id":"137","name":"Iran","country_codes":"ir","currency_id":"1","default_language_id":null,"priority":"137","status":"active"},{"id":"138","name":"Iraq","country_codes":"iq","currency_id":"1","default_language_id":null,"priority":"138","status":"active"},{"id":"22","name":"Ireland","country_codes":"ie","currency_id":"2","default_language_id":"1","priority":"10","status":"active"},{"id":"139","name":"Isle of Man","country_codes":"im","currency_id":"3","default_language_id":null,"priority":"139","status":"active"},{"id":"140","name":"Israel","country_codes":"il","currency_id":"1","default_language_id":null,"priority":"140","status":"active"},{"id":"15","name":"Italy","country_codes":"it","currency_id":"2","default_language_id":"21","priority":"12","status":"active"},{"id":"141","name":"Ivory Coast","country_codes":"ci","currency_id":"1","default_language_id":null,"priority":"141","status":"active"},{"id":"142","name":"Jamaica","country_codes":"jm","currency_id":"1","default_language_id":null,"priority":"142","status":"active"},{"id":"32","name":"Japan","country_codes":"jp","currency_id":"4","default_language_id":"38","priority":"32","status":"active"},{"id":"143","name":"Jersey","country_codes":"je","currency_id":"3","default_language_id":null,"priority":"143","status":"active"},{"id":"144","name":"Jordan","country_codes":"jo","currency_id":"1","default_language_id":null,"priority":"144","status":"active"},{"id":"145","name":"Kazakhstan","country_codes":"kz","currency_id":"1","default_language_id":null,"priority":"145","status":"active"},{"id":"146","name":"Kenya","country_codes":"ke","currency_id":"1","default_language_id":null,"priority":"146","status":"active"},{"id":"147","name":"Kiribati","country_codes":"ki","currency_id":"6","default_language_id":null,"priority":"147","status":"active"},{"id":"148","name":"Kosovo","country_codes":"kv","currency_id":"2","default_language_id":null,"priority":"148","status":"active"},{"id":"149","name":"Kuwait","country_codes":"kw","currency_id":"1","default_language_id":null,"priority":"149","status":"active"},{"id":"150","name":"Kyrgyzstan","country_codes":"kg","currency_id":"1","default_language_id":null,"priority":"150","status":"active"},{"id":"151","name":"Laos","country_codes":"la","currency_id":"1","default_language_id":null,"priority":"151","status":"active"},{"id":"152","name":"Latvia","country_codes":"lv","currency_id":"2","default_language_id":null,"priority":"152","status":"active"},{"id":"153","name":"Lebanon","country_codes":"lb","currency_id":"1","default_language_id":null,"priority":"153","status":"active"},{"id":"154","name":"Lesotho","country_codes":"ls","currency_id":"1","default_language_id":null,"priority":"154","status":"active"},{"id":"155","name":"Liberia","country_codes":"lr","currency_id":"1","default_language_id":null,"priority":"155","status":"active"},{"id":"156","name":"Libya","country_codes":"ly","currency_id":"1","default_language_id":null,"priority":"156","status":"active"},{"id":"157","name":"Liechtenstein","country_codes":"li","currency_id":"2","default_language_id":null,"priority":"157","status":"active"},{"id":"158","name":"Lithuania","country_codes":"lt","currency_id":"2","default_language_id":null,"priority":"158","status":"active"},{"id":"159","name":"Luxembourg","country_codes":"lu","currency_id":"2","default_language_id":null,"priority":"159","status":"active"},{"id":"160","name":"Macau","country_codes":"mo","currency_id":"1","default_language_id":null,"priority":"160","status":"active"},{"id":"161","name":"Macedonia","country_codes":"mk","currency_id":"2","default_language_id":null,"priority":"161","status":"active"},{"id":"162","name":"Madagascar","country_codes":"mg","currency_id":"1","default_language_id":null,"priority":"162","status":"active"},{"id":"163","name":"Malawi","country_codes":"mw","currency_id":"1","default_language_id":null,"priority":"163","status":"active"},{"id":"164","name":"Malaysia","country_codes":"my","currency_id":"1","default_language_id":null,"priority":"164","status":"active"},{"id":"165","name":"Maldives","country_codes":"mv","currency_id":"1","default_language_id":null,"priority":"165","status":"active"},{"id":"166","name":"Mali","country_codes":"ml","currency_id":"1","default_language_id":null,"priority":"166","status":"active"},{"id":"26","name":"Malta","country_codes":"mt","currency_id":"3","default_language_id":"1","priority":"26","status":"active"},{"id":"167","name":"Mariana Islands","country_codes":"mp","currency_id":"1","default_language_id":null,"priority":"167","status":"active"},{"id":"168","name":"Marshall Islands","country_codes":"mh","currency_id":"1","default_language_id":null,"priority":"168","status":"active"},{"id":"169","name":"Martinique","country_codes":"mq","currency_id":"2","default_language_id":null,"priority":"169","status":"active"},{"id":"170","name":"Mauritania","country_codes":"mr","currency_id":"1","default_language_id":null,"priority":"170","status":"active"},{"id":"171","name":"Mauritius","country_codes":"mu","currency_id":"1","default_language_id":null,"priority":"171","status":"active"},{"id":"172","name":"Mayotte Island","country_codes":"yt","currency_id":"2","default_language_id":null,"priority":"172","status":"active"},{"id":"173","name":"Mexico","country_codes":"mx","currency_id":"1","default_language_id":null,"priority":"173","status":"active"},{"id":"174","name":"Micronesia","country_codes":"fm","currency_id":"1","default_language_id":null,"priority":"174","status":"active"},{"id":"175","name":"Moldova","country_codes":"md","currency_id":"2","default_language_id":null,"priority":"175","status":"active"},{"id":"176","name":"Monaco","country_codes":"mc","currency_id":"2","default_language_id":null,"priority":"176","status":"active"},{"id":"177","name":"Mongolia","country_codes":"mn","currency_id":"1","default_language_id":null,"priority":"177","status":"active"},{"id":"178","name":"Montenegro","country_codes":"me","currency_id":"2","default_language_id":null,"priority":"178","status":"active"},{"id":"179","name":"Montserrat","country_codes":"ms","currency_id":"1","default_language_id":null,"priority":"179","status":"active"},{"id":"180","name":"Morocco","country_codes":"ma","currency_id":"1","default_language_id":null,"priority":"180","status":"active"},{"id":"181","name":"Mozambique","country_codes":"mz","currency_id":"1","default_language_id":null,"priority":"181","status":"active"},{"id":"182","name":"Myanmar","country_codes":"mm","currency_id":"1","default_language_id":null,"priority":"182","status":"active"},{"id":"11","name":"Namibia","country_codes":"na","currency_id":"1","default_language_id":"1","priority":"11","status":"active"},{"id":"183","name":"Nauru","country_codes":"nr","currency_id":"6","default_language_id":null,"priority":"183","status":"active"},{"id":"184","name":"Nepal","country_codes":"np","currency_id":"1","default_language_id":null,"priority":"184","status":"active"},{"id":"28","name":"Netherlands","country_codes":"nl","currency_id":"2","default_language_id":"1","priority":"20","status":"active"},{"id":"185","name":"Netherlands Antilles","country_codes":"an","currency_id":"1","default_language_id":null,"priority":"185","status":"active"},{"id":"186","name":"New Caledonia","country_codes":"nc","currency_id":"2","default_language_id":null,"priority":"186","status":"active"},{"id":"17","name":"New Zealand","country_codes":"nz","currency_id":"13","default_language_id":"1","priority":"17","status":"active"},{"id":"187","name":"Nicaragua","country_codes":"ni","currency_id":"1","default_language_id":null,"priority":"187","status":"active"},{"id":"188","name":"Niger","country_codes":"ne","currency_id":"1","default_language_id":null,"priority":"188","status":"active"},{"id":"189","name":"Nigeria","country_codes":"ng","currency_id":"1","default_language_id":null,"priority":"189","status":"active"},{"id":"190","name":"Niue","country_codes":"nu","currency_id":"1","default_language_id":null,"priority":"190","status":"active"},{"id":"191","name":"Norfolk Island","country_codes":"nf","currency_id":"6","default_language_id":null,"priority":"191","status":"active"},{"id":"192","name":"North Korea","country_codes":"kp","currency_id":"1","default_language_id":null,"priority":"192","status":"active"},{"id":"193","name":"Norway","country_codes":"no","currency_id":"2","default_language_id":null,"priority":"193","status":"active"},{"id":"194","name":"Oman","country_codes":"om","currency_id":"1","default_language_id":null,"priority":"194","status":"active"},{"id":"195","name":"Pakistan","country_codes":"pk","currency_id":"1","default_language_id":null,"priority":"195","status":"active"},{"id":"196","name":"Palau","country_codes":"pw","currency_id":"1","default_language_id":null,"priority":"196","status":"active"},{"id":"197","name":"Palestine","country_codes":"ps","currency_id":"1","default_language_id":null,"priority":"197","status":"active"},{"id":"198","name":"Panama","country_codes":"pa","currency_id":"1","default_language_id":null,"priority":"198","status":"active"},{"id":"199","name":"Papua New Guinea","country_codes":"pg","currency_id":"1","default_language_id":null,"priority":"199","status":"active"},{"id":"200","name":"Paraguay","country_codes":"py","currency_id":"1","default_language_id":null,"priority":"200","status":"active"},{"id":"201","name":"Peru","country_codes":"pe","currency_id":"1","default_language_id":null,"priority":"201","status":"active"},{"id":"202","name":"Philippines","country_codes":"ph","currency_id":"1","default_language_id":null,"priority":"202","status":"active"},{"id":"203","name":"Pitcairn","country_codes":"pn","currency_id":"1","default_language_id":null,"priority":"203","status":"active"},{"id":"204","name":"Poland","country_codes":"pl","currency_id":"2","default_language_id":null,"priority":"204","status":"active"},{"id":"30","name":"Portugal","country_codes":"pt","currency_id":"2","default_language_id":"33","priority":"30","status":"active"},{"id":"205","name":"Puerto Rico","country_codes":"pr","currency_id":"1","default_language_id":null,"priority":"205","status":"active"},{"id":"206","name":"Qatar","country_codes":"qa","currency_id":"1","default_language_id":null,"priority":"206","status":"active"},{"id":"207","name":"Reunion Island","country_codes":"re","currency_id":"2","default_language_id":null,"priority":"207","status":"active"},{"id":"8","name":"Romania","country_codes":"ro","currency_id":"2","default_language_id":"3","priority":"8","status":"active"},{"id":"208","name":"Russia","country_codes":"ru","currency_id":"2","default_language_id":null,"priority":"208","status":"active"},{"id":"209","name":"Rwanda","country_codes":"rw","currency_id":"1","default_language_id":null,"priority":"209","status":"active"},{"id":"210","name":"Saint Barth\u00e9lemy","country_codes":"bl","currency_id":"2","default_language_id":null,"priority":"210","status":"active"},{"id":"212","name":"Saint Kitts and Nevis","country_codes":"kn","currency_id":"1","default_language_id":null,"priority":"212","status":"active"},{"id":"213","name":"Saint Lucia","country_codes":"lc","currency_id":"1","default_language_id":null,"priority":"213","status":"active"},{"id":"214","name":"Saint Martin","country_codes":"mf","currency_id":"2","default_language_id":null,"priority":"214","status":"active"},{"id":"216","name":"Saint Vincent and The Grenadines","country_codes":"vc","currency_id":"1","default_language_id":null,"priority":"216","status":"active"},{"id":"217","name":"Samoa","country_codes":"ws","currency_id":"1","default_language_id":null,"priority":"217","status":"active"},{"id":"218","name":"San Marino","country_codes":"sm","currency_id":"2","default_language_id":null,"priority":"218","status":"active"},{"id":"219","name":"Sao Tome and Principe","country_codes":"st","currency_id":"1","default_language_id":null,"priority":"219","status":"active"},{"id":"220","name":"Saudi Arabia","country_codes":"sa","currency_id":"1","default_language_id":null,"priority":"220","status":"active"},{"id":"221","name":"Senegal","country_codes":"sn","currency_id":"1","default_language_id":null,"priority":"221","status":"active"},{"id":"222","name":"Serbia","country_codes":"rs","currency_id":"2","default_language_id":null,"priority":"222","status":"active"},{"id":"223","name":"Seychelles","country_codes":"sc","currency_id":"1","default_language_id":null,"priority":"223","status":"active"},{"id":"224","name":"Sierra Leone","country_codes":"sl","currency_id":"1","default_language_id":null,"priority":"224","status":"active"},{"id":"225","name":"Singapore","country_codes":"sg","currency_id":"1","default_language_id":null,"priority":"225","status":"active"},{"id":"226","name":"Sint Maarten","country_codes":"sx","currency_id":"2","default_language_id":null,"priority":"226","status":"active"},{"id":"227","name":"Slovakia","country_codes":"sk","currency_id":"2","default_language_id":null,"priority":"227","status":"active"},{"id":"228","name":"Slovenia","country_codes":"si","currency_id":"2","default_language_id":null,"priority":"228","status":"active"},{"id":"229","name":"Solomon Islands","country_codes":"sb","currency_id":"1","default_language_id":null,"priority":"229","status":"active"},{"id":"230","name":"Somalia","country_codes":"so","currency_id":"1","default_language_id":null,"priority":"230","status":"active"},{"id":"20","name":"South Africa","country_codes":"za","currency_id":"3","default_language_id":"1","priority":"20","status":"active"},{"id":"231","name":"South Georgia and the South Sandwich Islands","country_codes":"gs","currency_id":"3","default_language_id":null,"priority":"231","status":"active"},{"id":"232","name":"South Korea","country_codes":"kr","currency_id":"1","default_language_id":null,"priority":"232","status":"active"},{"id":"14","name":"Spain","country_codes":"es","currency_id":"2","default_language_id":"2","priority":"14","status":"active"},{"id":"233","name":"Sri Lanka","country_codes":"lk","currency_id":"1","default_language_id":null,"priority":"233","status":"active"},{"id":"211","name":"St Helena","country_codes":"sh","currency_id":"3","default_language_id":null,"priority":"211","status":"active"},{"id":"215","name":"St PIerre and Miquelon","country_codes":"pm","currency_id":"2","default_language_id":null,"priority":"215","status":"active"},{"id":"234","name":"Sudan","country_codes":"sd","currency_id":"1","default_language_id":null,"priority":"234","status":"active"},{"id":"235","name":"Suriname","country_codes":"sr","currency_id":"1","default_language_id":null,"priority":"235","status":"active"},{"id":"236","name":"Svalbard and Jan Mayen","country_codes":"sj","currency_id":"2","default_language_id":null,"priority":"236","status":"active"},{"id":"237","name":"Swaziland","country_codes":"sz","currency_id":"1","default_language_id":null,"priority":"237","status":"active"},{"id":"238","name":"Sweden","country_codes":"se","currency_id":"2","default_language_id":null,"priority":"238","status":"active"},{"id":"239","name":"Switzerland","country_codes":"ch","currency_id":"2","default_language_id":null,"priority":"239","status":"active"},{"id":"240","name":"Syria","country_codes":"sy","currency_id":"1","default_language_id":null,"priority":"240","status":"active"},{"id":"241","name":"Taiwan","country_codes":"tw","currency_id":"1","default_language_id":null,"priority":"241","status":"active"},{"id":"242","name":"Tajikistan","country_codes":"tj","currency_id":"1","default_language_id":null,"priority":"242","status":"active"},{"id":"243","name":"Tanzania","country_codes":"tz","currency_id":"1","default_language_id":null,"priority":"243","status":"active"},{"id":"244","name":"Thailand","country_codes":"th","currency_id":"1","default_language_id":null,"priority":"244","status":"active"},{"id":"246","name":"Togo","country_codes":"tg","currency_id":"1","default_language_id":null,"priority":"246","status":"active"},{"id":"247","name":"Tokelau","country_codes":"tk","currency_id":"1","default_language_id":null,"priority":"247","status":"active"},{"id":"248","name":"Tonga","country_codes":"to","currency_id":"1","default_language_id":null,"priority":"248","status":"active"},{"id":"249","name":"Trinidad and Tobago","country_codes":"tt","currency_id":"1","default_language_id":null,"priority":"249","status":"active"},{"id":"250","name":"Tunisia","country_codes":"tn","currency_id":"1","default_language_id":null,"priority":"250","status":"active"},{"id":"251","name":"Turkey","country_codes":"tr","currency_id":"2","default_language_id":null,"priority":"251","status":"active"},{"id":"252","name":"Turkmenistan","country_codes":"tm","currency_id":"1","default_language_id":null,"priority":"252","status":"active"},{"id":"253","name":"Turks and Caicos Islands","country_codes":"tc","currency_id":"1","default_language_id":null,"priority":"253","status":"active"},{"id":"254","name":"Tuvalu","country_codes":"tv","currency_id":"6","default_language_id":null,"priority":"254","status":"active"},{"id":"255","name":"Uganda","country_codes":"ug","currency_id":"1","default_language_id":null,"priority":"255","status":"active"},{"id":"256","name":"Ukraine","country_codes":"ua","currency_id":"2","default_language_id":null,"priority":"256","status":"active"},{"id":"257","name":"United Arab Emirates","country_codes":"ae","currency_id":"1","default_language_id":null,"priority":"257","status":"active"},{"id":"18","name":"United Kingdom","country_codes":"uk,gb","currency_id":"3","default_language_id":"1","priority":"16","status":"active"},{"id":"9","name":"United States","country_codes":"us","currency_id":"1","default_language_id":"1","priority":"18","status":"active"},{"id":"259","name":"Uruguay","country_codes":"uy","currency_id":"1","default_language_id":null,"priority":"259","status":"active"},{"id":"260","name":"US Minor Outlying Islands","country_codes":"um","currency_id":"1","default_language_id":null,"priority":"260","status":"active"},{"id":"261","name":"US Virgin Islands","country_codes":"vi","currency_id":"1","default_language_id":null,"priority":"261","status":"active"},{"id":"262","name":"Uzbekistan","country_codes":"uz","currency_id":"1","default_language_id":null,"priority":"262","status":"active"},{"id":"263","name":"Vanuatu","country_codes":"vu","currency_id":"1","default_language_id":null,"priority":"263","status":"active"},{"id":"264","name":"Vatican City","country_codes":"va","currency_id":"2","default_language_id":null,"priority":"264","status":"active"},{"id":"265","name":"Venezuela","country_codes":"ve","currency_id":"1","default_language_id":null,"priority":"265","status":"active"},{"id":"266","name":"Vietnam","country_codes":"vn","currency_id":"1","default_language_id":null,"priority":"266","status":"active"},{"id":"267","name":"Wallis and Futuna Islands","country_codes":"wf","currency_id":"2","default_language_id":null,"priority":"267","status":"active"},{"id":"268","name":"Western Sahara","country_codes":"eh","currency_id":"1","default_language_id":null,"priority":"268","status":"active"},{"id":"269","name":"Yemen","country_codes":"ye","currency_id":"1","default_language_id":null,"priority":"269","status":"active"},{"id":"270","name":"Zambia","country_codes":"zm","currency_id":"1","default_language_id":null,"priority":"270","status":"active"},{"id":"271","name":"Zimbabwe","country_codes":"zw","currency_id":"1","default_language_id":null,"priority":"271","status":"active"},{"id":"1","name":"Other","country_codes":"","currency_id":"1","default_language_id":null,"priority":"99","status":"active"}];
		var typeahead_zones = {};
		$.each(zone_countries, function (index, value) {
			typeahead_zones[value.name] = value.id;
		});
		$('.slider_input').html("<input type='text' class='none' style='width:0' id='country_destination' placeholder='" + messages.SEARCH_COUNTRY + "' autocomplete='off'/>");
		$('.slider_text').animate({opacity: 0, width: 0}, 300, function () {
			$('.slider_text').addClass('none')
		});
		var searchInput = $('#country_destination');
		var outerWidth = searchInput.attr('placeholder').length * 7;
		if (outerWidth < 125) outerWidth = 125;
		searchInput.removeClass('none').animate({opacity: 1, width: outerWidth, padding: '2px 5px'}, 300, function () {
			searchInput.focus()
		});
		searchInput.bind('blur', function () {
			$(this).val('');
			searchInput.animate({opacity: 0, width: 0, padding: 0}, 300, function () {
				searchInput.removeClass('focus').addClass('none');
			});
			$('.slider_text')
				.animate({opacity: 1, width: text_width}, 300)
				.removeClass('none')
				.css('display', '');//fix first time blur bug
		});
		searchInput.typeahead({
			updater: function (item) {
				window.location.href = url('/change_zone?zone=' + typeahead_zones[item]);
			},
			source: function (typeahead, query) {
				var search_array = [];
				for (var country_name in typeahead_zones) {
					search_array.push(country_name);
				}
				return search_array;
			}
		});
	});
</script>
@yield('script')

</body>
</html>