@extends('layouts.admin')

@section('style')
<style>
    body.homepage #content, body.how-to-top-up #content{
        background-color: unset
    }
    body.homepage #wrapper{
        background-color: unset;
        background-image: unset;
        z-index: 9900;
        padding-top: 16px;
    }
    body.homepage header, body.how-to-top-up header{
        background-color: rgba(2,122,195,1);
    }
    .submenu {
        border-top: unset;
    }
</style>
@endsection
@section('content')
<section id="topSection">
        <div id="wrapper" class="clearfix">
            @include('partials._submenu')
            <div id="content">
                <div class="container-fluid margin_vertical20">
                    <select class="phone_account_nav visible-phone width100 margin_bottom20">
                        <option value="account/home">Dashboard</option>
                        <option selected="" value="account/information">My Information</option>
                    </select>
                    <div class="row-fluid">
                        <div class="span6">
                                @include('partials._message')
                <div class="border_all margin_bottom30 padding20 clearfix">
                    <h2>Account Details</h2>
                    <div class="overflow_hidden change_info_dash">
                        <div class="clearfix">
                            <div class="width100 pull-left">
                                <div class="margin_bottom20 pull-left width20">Email:</div>
                                <div class="margin_bottom20 pull-left width60 long_email_container overflow_hidden"><span class="mobile_padding_left20">{{Auth::user()->email}}</span></div>
                                <div class="margin_bottom20 pull-left width20"><a href="#" class="myinfo_edit_email">Change</a></div>
                            </div>
                            <div class="width100 pull-left">
                                <div class="margin_bottom20 pull-left width20">Password:</div>
                                <div class="margin_bottom20 pull-left width60"><span class="mobile_padding_left20">xxxxxxxx</span></div>
                                <div class="margin_bottom20 pull-left width20"><a href="#" class="myinfo_edit_password">Change</a></div>
                            </div>                      
                        </div>
            <script>
            $(function() {
                $('.onoffswitch-checkbox')
                    .not('.parent-onoffswitch-checkbox, .onoffswitch-checkbox-obligatory')
                    .change(function() {
                        if (page_ajax.notification_toggle) {
                            page_ajax.notification_toggle.abort();
                        }
                        var _this = $(this);
                        var _notification_type, _notification_preference, _notification_checkbox_label;
                        _notification_type = $(this).attr('name');
                        _notification_checkbox_label = $(this)
                            .parent()
                            .find('.onoffswitch-label')
                            .not('.obligatory_active');
                        
                        if($(this).is(':checked')) {
                            _notification_preference = 'subscribe';
                            _notification_checkbox_label.addClass('active');
                        } else {
                            _notification_preference = 'unsubscribe';
                            _notification_checkbox_label.removeClass('active');
                        }
                        
                        page_ajax.notification_toggle = $.ajax({
                            url: url('process_notification'),
                            type: 'post',
                            dataType: 'json',
                            data: {
                                action: 'change_customer_subscription',
                                notification_type: _notification_type,
                                notification_preference: _notification_preference
                            },
                            success: function(response) {
                                if (response) {
                                    if (response.status == 'successful') {
                                        show_page_message(it('CHANGES_SAVED_MESSAGE'), 'status_success');
                                    } else {
                                        show_page_message(it('ERROR_TRY_AGAIN'), 'status_error');
                                        handle_error(_this, _notification_preference, _notification_checkbox_label);
                                    }
                                }
                                console.log(response);
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                show_page_message(it('ERROR_TRY_AGAIN'), 'status_error');
                                handle_error(_this, _notification_preference, _notification_checkbox_label);
                                console.log(jqXHR, textStatus, errorThrown);
                            }
                        });
                    });
                
                function handle_error(element, preference, label) {
                    if (preference == 'subscribe') {
                        label.removeClass('active');
                        element.prop('checked', false);
                    }
                    if (preference == 'unsubscribe') {
                        label.addClass('active');
                        element.prop('checked', true);
                    }
                }
            });
        </script>
                    </div>
                    <form id="change_credentials_email" class="clearfix" action="{{route('userChangeEmail')}}" method="post" novalidate="novalidate">
                        @csrf
                        <div id="change_email_form" class="row none">
                            <input type="password" id="orig_password" name="old_pass" placeholder="Current Password" autocomplete="off">
                            <input type="email" name="email" placeholder="New Email">
                            <input type="email" name="confirm_email" placeholder="Confirm Email" autocomplete="off">
                            <a href="#" class="change_info_back" style="line-height:42px;">Cancel</a>
                            <button type="submit" class="btn btn-primary pull-right">Save New Email</button>
                        </div>
                    </form>
                    <form id="change_credentials_password" class="clearfix" action="{{route('userChangePassword')}}" method="post" novalidate="novalidate">
                        @csrf
                        <div id="change_pass_form" class="row none">
                            <input type="password" id="orig_password" name="old_pass" placeholder="Current Password" autocomplete="off">
                            <input type="password" id="i_password" name="pass" placeholder="New Password" autocomplete="off">
                            <input type="password" name="confirm_pass" placeholder="Confirm password" autocomplete="off">
                            <a href="#" class="change_info_back" style="line-height:42px;">Cancel</a>
                            <button type="submit" class="btn btn-primary pull-right">Save New Password</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="span6">
                <div class="border_all margin_bottom30 padding20 clearfix">
        <h2>Personal Details</h2>
        <div>
            <div class="display_table_row">
                <div class="display_table_cell">Name:</div>
                <div class="display_table_cell padding_left20">{{Auth::user()->fname}} {{Auth::user()->lname}}</div>
            </div>
            <div class="display_table_row">
                <div class="display_table_cell">Phone:</div>
                <div class="display_table_cell padding_left20">{{Auth::user()->phone}}</div>
            </div>
            <div class="display_table_row">
                <div class="display_table_cell">Address:</div>
                <div class="display_table_cell padding_left20">{{@Auth::user()->profile->address}}</div>
            </div>
            <div class="display_table_row">
                <div class="display_table_cell">City:</div>
                <div class="display_table_cell padding_left20">{{@Auth::user()->profile->city}}</div>
            </div>
            <div class="display_table_row">
                <div class="display_table_cell">Zip Code:</div>
                <div class="display_table_cell padding_left20">{{@Auth::user()->profile->zip}}</div>
            </div>
            <div class="display_table_row">
                <div class="display_table_cell">Country:</div>
                <div class="display_table_cell padding_left20">{{@Auth::user()->profile->country}}</div>
            </div>
            <a href="#" class="myinfo_edit pull-right">Edit</a>
        </div>
        <form id="account_information" class="form-horizoantal clearfix none" action="{{route('userChangeProfile')}}" method="post" novalidate="novalidate">
            @csrf
            <div class="width49 pull-left">
                <input type="text" id="i_first_name" name="fname" placeholder="First Name" value="{{Auth::user()->fname}}" maxlength="50">
            </div>
            <div class="width49 margin_left2 pull-left">
                <input type="text" id="i_last_name" name="lname" placeholder="Last Name" value="{{Auth::user()->lname}}" maxlength="50">
            </div>
            <input type="tel" id="i_phone" name="phone" placeholder="Phone number" value="{{Auth::user()->phone}}" maxlength="50">
            <input type="text" id="i_address" name="address" placeholder="Address" value="{{@Auth::user()->profile->address}}" maxlength="100">
            <div class="width49 pull-left">
                <input type="text" id="i_city" name="city" placeholder="City" class="half_width" value="{{@Auth::user()->profile->city}}" maxlength="50">
            </div>
            <div class="width49 margin_left2 pull-left">
                <input id="i_zip" type="text" name="zip" placeholder="Zip Code" class="half_width_margin" value="{{@Auth::user()->profile->zip}}" maxlength="50">
            </div>
            <div class="width100 pull-left clear">
                <select name="country" id="i_country" class="half_width width100">
                    <option value="" selected="selected">Select country</option>
                    @foreach($country as $c)
                        <option value="{{$c->iso2}}" {{ $c->iso2 == Auth::user()->profile->country? 'selected':null }}>{{$c->name}}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary pull-right">Save</button>
        </form>
    </div>
            </div>
        </div>
    </div>
    <script>
    var page_notice = null;
    var page_errors = null;
    
    $(function () {
        
        $('.change_info_back').click(function() {
            $('.change_info_dash').removeClass('none');
            $('#change_pass_form').addClass('none');
             $('#change_email_form').addClass('none');
        });
        $('.myinfo_edit_password').click(function() {
            $('#change_pass_form').removeClass('none');
            $('.change_info_dash').addClass('none');
        });
        $('.myinfo_edit_email').click(function() {
            $('#change_email_form').removeClass('none');
            $('.change_info_dash').addClass('none');
        });
        $('.myinfo_edit').click(function() {
            $(this).parent().next().removeClass('none');
            $(this).parent().addClass('none');
        });
        $('.myinfo_edit_close').click(function() {
            $(this).parent().prev().removeClass('none');
            $(this).parent().addClass('none');
        });
    
        $('.myinfo_edit_timezone').click(function() {
            $('#change_timezone_form').removeClass('none');
            $('.change_info_dash').addClass('none');
            var current_timezone = $('#user_timezone').val();
            if (current_timezone != '') {
                $('#account_timezone').val(current_timezone);
            }
        });
        $('.change_timezone_back').click(function() {
            $('#change_timezone_form').addClass('none');
            $('.change_info_dash').removeClass('none');
        });
        $('#account_timezone').on('change', function() {
            var current_timezone = $(this).val();
            if (current_timezone != '') {
                $('#user_timezone').val(current_timezone);
            }
        });
    
        var password_validator = $("#change_credentials_password").validate({
            rules: {
                'orig_password': {
                    required: true
                },
                'password': {
                    required: true,
                    minlength: 7
                },
                'confirm_password': {
                    equalTo: "#change_credentials_password input[name=password]"
                }
            },
            messages: {
                password: {
                    required: messages.ERROR_ENTER_PASSWORD,
                    minlength: messages.ERROR_PASSWORD_TOO_SHORT
                },
                confirm_password: {
                    required: messages.ERROR_ENTER_PASSWORD_AGAIN,
                    equalTo: messages.ERROR_PASSWORD_MISMATCH
                }
            }
        });
    
        var email_validator = $('#change_credentials_email').validate({
            rules: {
                'orig_password': {
                    required: true
                },
                'email': {
                    required: true,
                    email: true
                },
                'confirm_email': {
                    equalTo:  $('#change_credentials_email input[name=email]')
                }
            }
        });
    
    
        var pers_info_validator = $('#account_information').validate({
            rules: {
                first_name: {required:true},
                last_name: {required:true},
                zip: {required: true},
                phone: {
                    required:true,
                    phone:true,
                    digits:true
                },
                address: {required:true},
                city: {required:true},
                state: {required:'#i_state:visible'},
                country: {required:true}
            },
            messages: {
                first_name: {
                    required: messages.ERROR_ENTER_FIRST_NAME
                },
                last_name: {
                    required: messages.ERROR_ENTER_LAST_NAME
                },
                phone: {
                    required: messages.ERROR_ENTER_PHONE_NUMBER,
                    phone: messages.ERROR_INVALID_PHONE_NUMBER
                },
                address: {
                    required: messages.ERROR_ENTER_ADDRESS
                },
                city: {
                    required: messages.ERROR_ENTER_CITY
                },
                state: {
                    required: messages.ERROR_ENTER_STATE
                },
                country: {
                    required: messages.ERROR_ENTER_COUNTRY
                }
            }
        });
        var cc_validator = $('#new_cc').validate({
            rules: {
                cc_select: { required: '#cc_select:visible' },
                cc_number: {
                    required: { depends: '#cc_number:visible' },
                    creditcard: { depends: '#cc_number:visible' }
                },
                cc_expiry_month: { required: '#cc_expiry_month:visible' },
                cc_expiry_year: { required: '#cc_expiry_year:visible' },
                cc_cvv: {
                    required: { depends: '#cc_cvv:visible' },
                    digits: { depends: '#cc_cvv:visible' }
                }
            }
        });
        $('#i_email').focus();
    });
    </script>
            </div><!--end #content -->
        </div><!--end #wrapper -->
    </section>
@endsection
