@extends('layouts.admin')

@section('style')
<style>
    body.homepage #content, body.how-to-top-up #content{
        background-color: unset
    }
    body.homepage #wrapper{
        background-color: unset;
        background-image: unset;
        z-index: 9900;
        padding-top: 16px;
    }
    body.homepage header, body.how-to-top-up header{
        background-color: rgba(2,122,195,1);
    }
    .submenu {
        border-top: unset;
    }
</style>
@endsection
@section('content')
<section id="topSection">
        <div id="wrapper" class="clearfix">
            @include('partials._submenu')
            <div id="content">
                <div class="container margin_bottom20 margin_top0">
                    <div class="margin_top20 visible-phone"><select class="phone_account_nav visible-phone width100 margin_bottom20">
                            <option value="account/home">Dashboard</option>
                                <option value="account/information">My Information</option>
                        </select>
                    </div>
                    <div class="accordion styled_acc row-fluid margin_top20" id="account_settings_acc">
                        <div class="accordion-group">
                            <div class="accordion-heading position_relative">
                                <a class="accordion-toggle styled_acc inactive open" data-toggle="collapse" href="#_account_settings_mobile_recharge">
                                        <span class="mobile_float_left bigicon-mr display_table_cell"></span>
                                        <span class="hidden-small-phone mobile_float_left mobile_padding_top15 display_table_cell valign_middle padding_left10 line_height1">Top Up</span>
                                    <div class="mobile_float_right tabs_more_info font-size18 line_height40">
                                        <span class="hidden-phone"></span>
                                        <div class="margin_left20 arrow_accordion_heading valign_middle"></div>
                                    </div>
                                </a>
                            </div>
                            <div id="_account_settings_mobile_recharge" class="accordion-body collapse in">
                                <div class="accordion-inner">
                                    <p>You may call it top up, mobile airtime, mobile credit, mobile load or whatever you want. You're in the right place to send credit to a mobile anywhere in the world.</p>
                                    <div>
                                        <p>Easy, fast, and reliable!</p>
                                        <a href="{{route('rechargeNow')}}" class="btn btn-primary">Recharge Now »</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer_apps_container">
                        <div class="background_white bg_cross_pattern border_top border_box pagination-centered hide_print padding_vertical40">
                    <span class="hidden-phone h_font font-size24 margin_right30 line_height1 valign_middle">
                                        Top up a mobile even easier with our free app					</span>
                                <a class="padding_horizontal15 footer_mobile_android" style="height: 50px; display: inline-block; vertical-align: middle" href="https://play.google.com/store/apps/details?id=com.topup.ui" target="_blank" rel="noopener" aria-label="Download the Android Calling app for Free" onclick="trackExternalLink(this, 'External Links', 'Clicks', 'Google Play'); return false;">
                            <svg style="height: 50px; width: 168px;" viewBox="0 0 135 40">
                                <use xlink:href="#svg_holder"></use>
                                <use xlink:href="#svg_stroke"></use>
                                <use xlink:href="#svg_GooglePlay"></use>
                            </svg>
                        </a>
                                        <span class="display_inlineblock mobile_margin_bottom20 mobile_width100 footer_mobile_ios footer_mobile_android"></span>
                                        <a class="padding_horizontal15 footer_mobile_ios" style="height: 50px; display: inline-block; vertical-align: middle" href="https://itunes.apple.com/qa/app/topup-com-easy-mobile-top-up/id1367920546?mt=8" target="_blank" rel="noopener" aria-label="Download the iOS Calling app for Free" onclick="trackExternalLink(this, 'External Links', 'Clicks', 'App Store'); return false;">
                            <svg style="height: 50px; width: 168px;" viewBox="0 0 135 40">
                                <use xlink:href="#svg_holder"></use>
                                <use xlink:href="#svg_stroke"></use>
                                <use xlink:href="#svg_AppleStore"></use>
                            </svg>
                        </a>
                        </div>
                </div>
            </div><!--end #content -->
        </div><!--end #wrapper -->
    </section>
@endsection
