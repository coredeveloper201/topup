@extends('layouts.admin')

@section('style')
<style>
    body.homepage #content, body.how-to-top-up #content{
        background-color: unset
    }
    body.homepage #wrapper{
        background-color: unset;
        background-image: unset;
        z-index: 9900;
        padding-top: 16px;
    }
    body.homepage header, body.how-to-top-up header{
        background-color: rgba(2,122,195,1);
    }
    .submenu {
        border-top: unset;
    }
</style>
@endsection
@section('content')
<section id="topSection">
        <div id="wrapper" class="clearfix">
            @include('partials._submenu')
            <div id="content">
                <div class="container-fluid margin_vertical20">
                    <div class="row-fluid">
                        @include('partials._message')
                        <br><br>
                        <h1 style="text-align: center">Register Vendor</h1>
                        <br>
                    </div>
                    <form id="change_credentials_password" action="{{ route('postUpdateVendor', $user->id) }}" method="post">
                    @csrf
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="border_all margin_bottom30 padding20 clearfix">
                                <h2>Basic Informations</h2>
                                <div class="overflow_hidden change_info_dash">
                                    <div class="clearfix">
                                        <div id="change_pass_form" class="row">
                                            <input type="text" name="fname" placeholder="First Name" value="{{ $user->fname }}" maxlength="50">
                                            <input type="text" name="lname" placeholder="Last Name" value="{{ $user->lname }}" maxlength="50">
                                            <input type="text" name="phone" placeholder="Phone Number" value="{{ $user->phone }}" maxlength="50">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="border_all margin_bottom30 padding20 clearfix">
                                <div class="overflow_hidden change_info_dash">
                                    <div class="clearfix">
                                        <div id="change_pass_form" class="row">
                                            <h2>Vendor Domain</h2>
                                            <input type="text" name="domain" placeholder="https://mediusware.com" value="{{ $user->domain }}">
                                            <hr>
                                            <h2>Payment Methods</h2>
                                            <div class="checkbox">
                                                @foreach($methods as $m)
                                                    <label><input type="checkbox" name="{{ $m->name }}" value="{{ $m->name }}" {{ in_array($m->name, $user->methods->pluck('name')->toArray()) ?'checked':null }}>{{ $m->name }}</label>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="submit_holder pagination-centered">
                            <button type="submit" class="btn btn-primary">Update Vendor</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div><!--end #content -->
        </div><!--end #wrapper -->
</section>
@endsection