@extends('layouts.admin')

@section('style')
<style>
    body.homepage #content, body.how-to-top-up #content{
        background-color: unset
    }
    body.homepage #wrapper{
        background-color: unset;
        background-image: unset;
        z-index: 9900;
        padding-top: 16px;
    }
    body.homepage header, body.how-to-top-up header{
        background-color: rgba(2,122,195,1);
    }
    .submenu {
        border-top: unset;
    }
</style>
@endsection
@section('content')
<section id="topSection">
        <div id="wrapper" class="clearfix">
            @include('partials._submenu')
            <div id="content">
                <div class="container-fluid margin_vertical20">
                    <div class="row-fluid">
                        @include('partials._message')
                        <br><br>
                        <h1 style="text-align: center">Register Vendor</h1>
                        <br>
                    </div>
                    <form id="change_credentials_password" action="{{ route('postCreateVendor') }}" method="post">
                    @csrf
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="border_all margin_bottom30 padding20 clearfix">
                                <h2>Basic Informations</h2>
                                <div class="overflow_hidden change_info_dash">
                                    <div class="clearfix">
                                        <div id="change_pass_form" class="row">
                                            <input type="text" name="fname" placeholder="First Name" value="{{ old('fname') }}" maxlength="50">
                                            <input type="text" name="lname" placeholder="Last Name" value="{{ old('lname') }}" maxlength="50">
                                            <input type="email" name="email" placeholder="Email" value="{{ old('email') }}" maxlength="50">
                                            <input type="password" name="password" placeholder="Password">
                                            <input type="password" name="password_confirmation" placeholder="Confirm password">
                                            <input type="tel" id="i_phone" name="phone" placeholder="Phone number" maxlength="50">
                                            <input type="text" id="i_address" name="address" placeholder="Address" maxlength="200">
                                            <div class="width49 pull-left">
                                                <input type="text" id="i_city" name="city" placeholder="City" class="half_width" maxlength="50">
                                            </div>
                                            <div class="width49 margin_left2 pull-left">
                                                <input id="i_zip" type="text" name="zip" placeholder="Zip Code" class="half_width_margin" maxlength="50">
                                            </div>
                                            <div class="width100 pull-left clear">
                                                <select name="country" id="i_country" class="half_width width100">
                                                    <option value="" selected="selected">Select country</option>
                                                    @foreach($data['cous'] as $c)
                                                        <option value="{{$c->iso2}}">{{$c->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="border_all margin_bottom30 padding20 clearfix">
                                <div class="overflow_hidden change_info_dash">
                                    <div class="clearfix">
                                        <div id="change_pass_form" class="row">
                                            <h2>Vendor Domain</h2>
                                            <input type="text" name="domain" placeholder="https://mediusware.com">
                                            <hr>
                                            <h2>Payment Methods</h2>
                                            <div class="checkbox">
                                                @foreach($methods as $m)
                                                    <label><input type="checkbox" name="{{ $m->name }}" value="{{ $m->name }}">{{ $m->name }}</label>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="submit_holder pagination-centered">
                            <button type="submit" class="btn btn-primary">Create Vendor account</button>
                        </div>
                    </div>
                    </form>
                    <br><br>
                    <div class="row-fluid">
                        <div class="span6">
                            <table class="table">
                                <h1>All Vendors</h1>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>phone</th>
                                        <th>Domain</th>
                                        <th>Payment Methods</th>
                                        <th>#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($vendors as $key => $user)
                                        <tr>
                                            <th scope="row">{{$key + 1}}</th>
                                            <td>{{$user->fname}} {{$user->lname}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->phone}}</td>
                                            <td>{{$user->domain}}</td>
                                            <td>
                                                @foreach($user->methods as $met)
                                                <span class="label label-success">{{$met->name}}</span>
                                                @endforeach
                                            </td>
                                            <td>
                                                <a  class="btn btn-success" href="{{ route('adminVendorEdit', $user->id) }}"><i class="fa fa-eye"></i></span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div><!--end #content -->
        </div><!--end #wrapper -->
</section>
@endsection