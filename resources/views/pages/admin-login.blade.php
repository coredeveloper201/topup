@extends('layouts.page')

@section('style')
<style>
    body.homepage #content, body.how-to-top-up #content{
        background-color: unset
    }
    body.homepage #wrapper{
        background-color: unset;
        background-image: unset;
    }
    body.homepage header, body.how-to-top-up header{
        background-color: rgba(2,122,195,1);
    }
</style>
@endsection
@section('content')

<section id="topSection">
	<div id="wrapper" class="clearfix">
					<div id="content">
			
<div class="container-fluid margin_bottom20">
	<div class="row-fluid">
		<div class="span6 margin_top20">
            <h1>Admin Login</h1>
            @include('partials._message')
			<form method="POST" action="{{ route('login') }}" id="account_login_form">
				@csrf
				<input type="email" name="email" placeholder="Email">
				<div class="input-append">
			   		<input type="password" name="password" placeholder="Password" class="width60"><button type="submit" class="width40 btn btn-primary mobile_button">Log in »</button>
				</div>
								<div class="pagination-right">
				</div>
									<input type="hidden" name="login_action" value="login">
							</form>
		</div>
	</div>
</div>
		</div><!--end #content -->
	</div><!--end #wrapper -->
</section>

@endsection
