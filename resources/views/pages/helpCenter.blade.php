@extends('layouts.page')

@section('style')
<style>
    body.homepage #content, body.how-to-top-up #content{
        background-color: unset
    }
    body.homepage #wrapper{
        background-color: unset;
        background-image: unset;
    }
    body.homepage header, body.how-to-top-up header{
        background-color: rgba(2,122,195,1);
    }
</style>
@endsection
@section('content')
<section id="topSection">
        <div id="wrapper" class="clearfix">
                @if(Auth::check())
                @if(Auth::user()->role = 'user')
                    @include('partials._submenu')
                @endif
                @endif
                    <div id="content">
                <div class="container-fluid">
        <div class="row-fluid">
            <h1 class="margin_top20">Help Center</h1>
        </div>
    </div>
    <div class="container-fluid margin_bottom20 margin_top10 overflow_hidden">
        <div class="row-fluid">
            <div class="span4 hidden-phone">
                <ul class="qanda_big_arrows margin0">
                    <li data-catname="51" class="mainMnu">Top Up</li>
                    <li data-catname="17" class="mainMnu">How to Buy</li>
                    <li data-catname="29" class="mainMnu active">Payment </li>
                    <!-- <li data-catname="15" class="mainMnu">Account Information</li> -->
                    <!-- <li data-catname="60" class="mainMnu">Offers &amp; Promotions</li> -->
                    <li data-catname="19" class="mainMnu">Company Background</li>
                </ul>
            </div>
            <div class="span8">
                <ul class="faq_list">
                                        <li class="cat_title">Payment </li>
                                                <li data-catname="29" data-target="#851" data-id="851" data-toggle="collapse" class="collapsed active">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">What forms of payment do you accept?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="851">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">The payment can be made with Credit Card, Debit Card and PayPal. </div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="29" data-target="#845" data-id="845" data-toggle="collapse" class="collapsed active">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Where can I find my invoice? </span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="845">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">Log into your online account, and go to the "Activity" tab. Next, tap on "Orders" and look for the order you're interested in. On the same line, tap on "View" to check your invoice details. <br><br>
    If you need to check older transactions, please hit "More" below your list of orders to have the whole list displayed. </div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="29" data-target="#185" data-id="185" data-toggle="collapse" class="collapsed active">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Do you have Terms and Conditions?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="185">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">Yes, our products are subject to certain <a href="/terms">Terms and Conditions</a>. Once you've made a purchase, it means you have agreed to them.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="29" data-target="#387" data-id="387" data-toggle="collapse" class="collapsed active">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">What is 3D-Secure?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="387">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">3-D Secure is a global e-commerce solution that enables cardholders to authenticate themselves to their card issuer through the use of a unique personal code/password. When you make a purchase, you may be redirected briefly to your bank/issuer website to enter your 3D-Secure password and when your bank/issuer accepts this, then you are redirected back to our website to complete the purchase. This solution addresses current consumer concerns about the security of online shopping and the high rate of e-commerce fraud and it is designed to take online shopping security and consumer confidence to a new level.<br>
    For VISA customers the security check will appear under the name "Verified by VISA". For MASTERCARD customers the security check will appear under the name "MasterCard SecureCode".</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="29" data-target="#389" data-id="389" data-toggle="collapse" class="collapsed active">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Why have I been asked to enter a password when making a purchase?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="389">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">This is a 3D-Secure security check directly from your bank/card issuer. When you make a purchase, you may be redirected briefly to your bank/issuer website to enter your 3D-Secure password and when your bank/issuer accepts this, then you are redirected back to our website to complete the purchase. If the bank/issuer is asking you to enroll for 3D secure or enter a password for 3D-Secure and you want to skip this, then you must get in touch with the bank directly.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="29" data-target="#169" data-id="169" data-toggle="collapse" class="collapsed active">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">What is a “Pending” transaction?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="169">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">A “Pending” transaction is any transaction waiting to be processed. On your credit card statement it may appear as a pending debit and this will turn to a full debit as soon as your order changes from “Pending” to “Successful”. If your order does not change to “Successful”, then the pending debit will automatically disappear from your credit card statement. You are only charged for “Successful” orders.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="29" data-target="#153" data-id="153" data-toggle="collapse" class="collapsed active">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">What credit/debit cards do you accept?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="153">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">We accept Visa, MasterCard, American Express and Discover for orders on the website. You can also use your PayPal account with us. Please note that American Express cards can be used for USD payments only.
    </div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="29" data-target="#191" data-id="191" data-toggle="collapse" class="collapsed active">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Can I buy your products using a debit card?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="191">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">You can purchase any of our products by using both credit and debit cards.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="29" data-target="#149" data-id="149" data-toggle="collapse" class="collapsed active">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Is your website secure? Can I place orders with my card safely?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="149">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">Yes, our website is <a href="/privacy">secure</a> for online transactions. We use reasonable precautions to keep the personal information you disclose both in your browsing and your purchases, and never release this information to third parties.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="29" data-target="#155" data-id="155" data-toggle="collapse" class="collapsed active">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">What's my billing address?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="155">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">Your billing address is the address your card was issued to. Please use this as your billing address for orders on the website. We will not send anything by mail to this address.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="29" data-target="#189" data-id="189" data-toggle="collapse" class="collapsed active">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Can I ask for my money back?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="189">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">We offer 100% guarantee. However, our products are subject to <a href="/terms">Terms and Conditions</a> and we will offer a refund only if the service did not work.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                        
                                        <li class="cat_title">Top Up</li>
                                                <li data-catname="51" data-target="#367" data-id="367" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Where can I find top up promotions?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="367">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">You can check all our Top Up promotions on our <a href="/buy/mobile_recharge">Top Up page</a>, by selecting the operator of the number you want to recharge.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#291" data-id="291" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">What is TopUp?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="291">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">Top Up is a service that allows you to <a href="/buy/mobile_recharge">send credit to any mobile phone</a> in the world. It is fast, simple and reliable!</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#393" data-id="393" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">How can I make a top up?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="393">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">To recharge a mobile abroad, first you need to <b>open a free account</b> on this website. <br><br>Then, follow these simple steps:<br><ol><li>go to the <a href="/buy/mobile_recharge" target="blank">online ordering form</a></li> <li> choose the country where you send the recharge and the operator of the mobile you want to recharge</li> <li>proceed to payment</li><li>check the recharge status in the Activity - Orders section of your account.</li></ol></div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#307" data-id="307" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Is there an expiry date on the Top Up that I send?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="307">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">We do not impose any expiration date on the Top Up that you send and normally the mobile operator of the phone you are recharging will not impose any either. We advise you check such details with your mobile operator. </div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#208" data-id="208" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">How long does it take for the top up to arrive?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="208">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">The Top Up credit is normally received immediately after a successful purchase.<br> However, on rare occasions, if the mobile operator of the prepaid number you selected to recharge is experiencing delays, then the Top Up could take up to 12 hours.<br><br> Note that the operator will not always send an SMS to the recipient to advise of the top up and so if, after 12 hours the Top Up has not been received, then the recipient should get in touch directly with the mobile operator directly to confirm their balance.<br> The contact information for the operator is listed on the invoice, available in <a href="/account/activity?product=mobile_recharge&amp;tab=orders">your account</a>. Alternatively, you can let us know after 24 hours, so that we can contact the operator for you.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#853" data-id="853" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">What are "local taxes"? </span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="853">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">A local tax is tax assessed and levied by a local entity or authority such as a state, county, municipality, or a local operator in this case. Local taxes differ according to the the country airtime is sent to. They may cover one, several or all of the taxes below: local operator's fees, VAT, specific municipality taxes, or national taxes in the destination country. The local taxes are not under the control of . The processing fee is what  adds to the order value to cover for international transaction costs. The service processing fee will be displayed in the checkout, which is the last step in the purchase process. 
    </div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#657" data-id="657" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">What is Nauta?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="657">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">Nauta is the Internet provider in Cuba, offering Wi-Fi access in hotspots all over the country. </div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#659" data-id="659" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">How can I recharge a Nauta account?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="659">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">To recharge a Nauta account, you need to visit the <a href="/buy/mobile_recharge">Top Up page</a> and select Nauta. Then enter the email address associated to your friend's account and choose the amount you want to send.  Tick the Send SMS box, enter your friend's mobile number, and type in a message if you want to let them know you recharged their Internet account.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#303" data-id="303" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Why did the phone I recharged not receive the promotional bonus?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="303">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">If the bonus was not received, you or the beneficiary should get in touch with the mobile operator directly. The contact information is available on your <a href="/account/activity?product=mobile_recharge&amp;tab=orders">invoice</a> with us. Alternatively, you can let us know after 24 hours, so that we can contact the operator for you.<br><br>
    
    The promotions that we display on our website are run solely by the local mobile operators, who are fully responsible for awarding the bonuses. We also display their Terms and Conditions associated with a specific promotion.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#305" data-id="305" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Why did the mobile phone I recharged receive less credit than I expected?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="305">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">In many countries, the government charges a local sales tax on Top Ups. It is the same tax that consumers would have to pay if they bought a Top Up in store, in that particular country. Also note that mobile operators in several countries reserve the right to impose fees on top ups without prior notice.
    </div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#61" data-id="61" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Can I recharge any mobile?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="61">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">You must check if the mobile you wish to recharge is a prepaid number
    and allows prepaid recharges. After this, please make sure you have the
    correct number and mobile operator selected.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#205" data-id="205" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">How do I know if my Top Up was processed?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="205">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">The Top Up is processed once you receive the invoice with the status “Successful.” This means that the amount was sent to the mobile operator. It may take up to 24 hours for the mobile operator to update the credit to the prepaid number you selected. You can always check the status of your orders in <a href="/account/home">your account</a>.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#289" data-id="289" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Are there any extra fees that apply to top ups?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="289">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">For every Top Up you will be charged a processing fee of minimum $1, depending on the amount you are recharging. You can see the exact processing fee amount on your checkout order summary before completing the purchase.  </div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#301" data-id="301" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">What can I do if I recharged the wrong number?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="301">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">Unfortunately, we cannot retrieve recharges that were sent to the wrong number or operator. These transactions are considered complete and we cannot retrieve or refund them since a successful online payment leads to immediate credit availability on the recharged mobile. Please carefully check and double-check the number you enter.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#855" data-id="855" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Can I recharge minutes or data with Mobile Recharge service?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="855">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">We can help you top up a mobile online, but the way the credit is being used depends on the local provider. You may want to check if the mobile operator has any plans or Internet packages advertised on our website. </div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#109" data-id="109" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">What is the calling rate that will apply to my Top Up beneficiary?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="109">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">The rates that apply to calls made by the mobile number you recharge will be the ones established by the local mobile provider. Please check this information with that mobile provider.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#207" data-id="207" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Where can I check the Terms and Conditions for a Top Up offer?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="207">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">{{$_SERVER['SERVER_NAME']}} may have special offers for different mobile operators. Please check the Terms and Conditions displayed on the <a href="/buy/mobile_recharge">Top Up page</a> to make sure you know all the details of an ongoing promotion.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#210" data-id="210" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">What if my Top Up order has the status “Failed”?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="210">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">The “Failed” status can result if the mobile operator in that country is experiencing a technical outage. In this case we will attempt to send the recharge to that operator over the following 12 hours during which time the mobile operator will most likely have recovered and be able to accept the recharge. The status will then charge to “Successful”.<br><br>
    
    The “Failed” status can also result for: incorrect mobile number, incorrect operator-number combination entered, post-paid instead of prepaid mobile number entered, mobile number is not yet activated etc. Please recheck the information you entered to make sure all information is valid.<br><br>
    
    If a “Failed” status changes to “Successful” then you will be changed accordingly. If after 72 hours, the status remains on “Failed” and you have not requested an inquiry, then our system will automatically cancel the transaction and you will not be charged.
    
    You can always <b>check the status of your orders</b> in your account <a href="/account/activity?product=mobile_recharge&amp;tab=orders">Activity</a>. </div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#331" data-id="331" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Is there a maximum order limit for top ups?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="331">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">There is no maximum order limit for the number of orders a client can place. <br><br>
    Still, some local operators may set a limit to the top up amount that a number can be recharged per week or month.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#857" data-id="857" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Will the customers be able to use Cubacel bonuses to buy the 3G plans?
    </span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="857">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">No, unfortunately, the customers can use only the main Cubacel balance to buy the 3G plans.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="51" data-target="#859" data-id="859" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Can I send mobile recharges that can be used for 3G services in Cuba?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="859">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">Yes, the credit you send to Cuba can be used for purchasing 3G plans. Just remember that during Cubacel promotions, only the main balance can be used for buying such plans. 
    </div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                        
                                        <li class="cat_title">How to Buy</li>
                                                <li data-catname="17" data-target="#795" data-id="795" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">How can I change the card saved in my account for a recurring payment?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="795">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">In order to change the card saved in your account for a recurring payment, you need to:
    <ul class=" list_style margin_left10 padding_left10">
    <li>Save a new card in your account <a href="/account/information">My Information section</a> (Payment Methods &gt;&gt; Add a new card);</li>
    <li>Go to <a href="/account/home">My Account</a>, choose the product for which you want to replace the card, and select the new card from the dropdown.</li>
    </ul> </div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="17" data-target="#793" data-id="793" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">How can I save another credit/debit card in my account?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="793">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7"><a href="/account/information">Log in</a> to your account, click on "My Information" tab and then "Add a new card" under "Payment Method." </div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="17" data-target="#145" data-id="145" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Do you charge anything extra?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="145">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">For buying a Top Up you will be charged a small processing fee depending on the value you wish to buy. The exact amount will appear on the checkout page before you make payment.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="17" data-target="#401" data-id="401" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Why was my payment declined?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="401">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">Declines can happen for a variety of reasons, the most common of which are: insufficient funds, incorrect card information, expired or canceled card, and the impossibility of making online payments with that specific card. In some rare cases there may be a communication error or a technical problem, so we recommend that you try again later. If your payment still doesn't go through, please contact your bank for more information.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="17" data-target="#860" data-id="860" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Why is there a processing fee? </span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="860">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">The processing fee is a percentage-based fee that's charged on every transaction. It reflects several international transactions we need to cover to make possible your fast and secure online top ups. The value is of minimum $1, depending on the amount you are recharging. You can see the exact processing fee amount on your checkout order summary before completing the purchase. </div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="17" data-target="#862" data-id="862" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Why is there a processing fee? </span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="862">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">The processing fee is a percentage-based fee that's charged on every transaction. It reflects several international transactions we need to cover to make possible your fast and secure online top ups. The value is of minimum $1, depending on the amount you are recharging. You can see the exact processing fee amount on your checkout order summary before completing the purchase. </div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="17" data-target="#161" data-id="161" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Can I pay through money transfer?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="161">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">No, but we accept all major cards and PayPal. Please check our payment options at the bottom of the page.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="17" data-target="#167" data-id="167" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Is there any other place I can buy your service from?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="167">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">No, the service can only be bought online, on this website.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="17" data-target="#157" data-id="157" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">How do I edit my billing address?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="157">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">If your billing address has recently changed, go to <a href="/account/information">My Information page</a>, click Edit, delete the old address and add the new one. </div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="17" data-target="#187" data-id="187" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Do I sign a contract?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="187">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">To buy the service, you must agree to our <a href="/terms">Terms and Conditions</a>. This will be considered your consent for buying the products.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                        
                                        <li class="cat_title">Account Information</li>
                                                <li data-catname="15" data-target="#129" data-id="129" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">I lost my password. How can I retrieve it?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="129">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">On the <a href="/account/login">login page</a> click <a href="/account/forgot_password">Forgot password</a>, enter the email registered in your account and click Retrieve. You will receive a link in your email inbox. Click the link to change your password.  If you cannot remember the email address registered in your account, please <a href="/contact">contact us</a>.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="15" data-target="#127" data-id="127" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">How do I change my password?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="127">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">To change your password log in to your account, click on <a href="/account/information">My Information</a>, edit your password and click Save. </div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                        
                                        <li class="cat_title">Offers &amp; Promotions</li>
                                                <li data-catname="60" data-target="#361" data-id="361" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">How can I find out about offers and promotions?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="361">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">You will receive updates about promotions and coupons by email, if you are subscribed to our Newsletter.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                        
                                        <li class="cat_title">Company Background</li>
                                                <li data-catname="19" data-target="#179" data-id="179" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">Where is your business located?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="179">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">Our business is registered in Atlanta, GA, USA and we have offices in USA, South America and Europe.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="19" data-target="#195" data-id="195" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">What is your mailing address?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="195">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">Our mailing address is available on our <a href="/contact">Support page</a>. Please find it there.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                                <li data-catname="19" data-target="#193" data-id="193" data-toggle="collapse" class="collapsed">
                            <div class="clearfix pagination-right cursor_pointer">
                                <span class="pull-left pagination-left width98">How long have you been in business?</span>
                                <i class="arrow_left"></i>
                            </div>
                            <div class="collapse row-fluid" id="193">
                                <div class="span1 hidden-phone">
                                    <div class="info_arrow_box pull-right pagination-centered">i</div>
                                </div>
                                <div class="span7">Our company was launched in 2002. Since then we have been offering our services to hundreds of thousands of customers worldwide.</div>
                                <div class="margin_left50">
                                                            </div>
                            </div>
                        </li>
                                        
                                </ul>
            </div>
        </div>
    </div>
    <script>
    var help_center_question = "";
    $(function() {
        $('.qanda_big_arrows li:first-child').click();
        if (help_center_question)
            button_search();
    })
    </script>		</div><!--end #content -->
        </div><!--end #wrapper -->
    </section>
@endsection
@section('script')
<script>
$('.mainMnu').click(function(){
    $('.mainMnu').removeClass('active');
    var catname = $(this).data('catname');
    $(this).addClass('active');
    $('.collapsed').hide();
    $('.collapsed')
        .filter(function(){
            return $(this).data('catname') === catname;
        })
        .show();
    
        catname
})
</script>
@endsection