@extends('layouts.vendor')

@section('style')
<style>
    body.homepage #content, body.how-to-top-up #content{
        background-color: unset
    }
    body.homepage #wrapper{
        background-color: unset;
        background-image: unset;
    }
    body.homepage header, body.how-to-top-up header{
        background-color: rgba(2,122,195,1);
    }
    .center{
        text-align: center
    }
</style>
@endsection
@section('content')
<section id="topSection">
        <div id="wrapper" class="clearfix">
            @include('partials._submenu')
            <div id="content">
                <div  class="container-fluid margin_vertical20">
                    <div class="row-fluid margin_top20">
                        <div class="col-md-12">
                            @include('partials._message')
                            <table class="table">
                                <h1 class="center">All Users</h1><br>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>phone</th>
                                        <th>Email</th>
                                        {{-- <th>Disabled</th> --}}
                                        <th>#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($users as $key => $user)
                                        <tr>
                                            <th scope="row">{{$key + 1}}</th>
                                            <td>{{$user->fname}}</td>
                                            <td>{{$user->lname}}</td>
                                            <td>{{$user->phone}}</td>
                                            <td>{{ substr($user->email, strpos($user->email, '-')+1) }}</td>
                                            {{-- <td><a href="{{route('userChangeStat', $user->id)}}" class="btn {{$user->status ? 'btn-success':'btn-danger'}} btn-sm">{{$user->status ? 'Enabled':'Disabled'}}</a></td> --}}
                                            <td>
                                                <a  class="btn btn-success" href="{{ route('vendorUser', $user->id) }}"><i class="fa fa-eye"></i></span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div><!--end #content -->
        </div><!--end #wrapper -->
    </section>
@endsection

@section('script')

<script>

</script>
@endsection
