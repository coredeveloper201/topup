@extends('layouts.vendor')

@section('style')

<style>
    body.homepage #content, body.how-to-top-up #content{
        background-color: unset
    }
    body.homepage #wrapper{
        background-color: unset;
        background-image: unset;
    }
    body.homepage header, body.how-to-top-up header{
        background-color: rgba(2,122,195,1);
    }
textarea{  
  /* box-sizing: padding-box; */
  overflow:hidden;
  /* demo only: */
  padding:10px;
  width:250px;
  font-size:14px;
  margin:50px auto;
  display:block;
  border-radius:unset;
  border:unset !important;
  webkit-box-shadow: unset !important;
  -moz-box-shadow: unset !important;

  border-top: unset;
  background-color: #FAFAFA;
}
textarea:focus {
    outline: unset;
    border-color: unset;
    -webkit-box-shadow: unset;
    -moz-box-shadow: unset;
    box-shadow: unset;
}

.pagination span {
    float: left;
    padding: 0 14px;
    line-height: 34px;
    text-decoration: none;
    border: 1px solid #ddd;
    border-left-width: 0;
}
.active span{
    background-color: coral;
}
</style>
@endsection
@section('content')
<section id="topSection">
        <div id="wrapper" class="clearfix">
            @include('partials._submenu')
            <div id="content">
                <div  class="container-fluid margin_vertical20">
                    <div class="row-fluid margin_top20">
                        <div class="col-md-12">
                            <div class="row">
                                    <select name="country" class="margin0 width45" onchange="location = this.value;" style="float: left">
                                        <option value="">Select country</option>
                                        @foreach($countries as $country)
                                            <option value="{{ route('vendorPricing', [$country->id]) }}" {{ $c ? $c == $country->id ? 'selected':null:null }}>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                    <select name="operator" class="margin0 width45" onchange="location = this.value;" style="float: left">

                                        @if($operators)
                                            @foreach($operators as $operator)
                                                <option value="{{ route('vendorPricing', [$operator->country_id,$operator->id]) }}" {{ $o ? $o == $operator->id ? 'selected':null:null }}>{{$operator->name}}</option>
                                            @endforeach
                                        @else
                                            <option value="">Select Operators</option>
                                        @endif
                                    </select>
                            </div>
                            <br><br>
                            @include('partials._message')
                            <table class="table">
                                <h2> Manage Price </h2>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Country</th>
                                        <th>Operator</th>
                                        <th>ISO2 Name</th>
                                        <th>Currency</th>
                                        <th>Product Currency</th>
                                        <th>Product</th>
                                        <th>Price</th>
                                        <th>Custom Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $key => $product)
                                        <tr>
                                            <th scope="row">{{$key + 1}}</th>
                                            <td>{{@$product->operator->country->name}}</td>
                                            <td>{{$product->operator->name}}</td>
                                            <td>{{@$product->operator->country->iso2}}</td>
                                            <td>{{@$product->operator->country->currency}}</td>
                                            <td>{{@$product->pcur}}</td>
                                            <td>{{$product->product}}</td>
                                            <td id="pricePlot-{{$product->id}}">{{$product->price}}</td>
                                            <td>
                                                <input id="customPrice-{{$product->id}}" type="text" name="price" value="{{ $product->vendorPrice['price'] }}" placeholder="null" style="width:100px">
                                                <input id="customPriceId-{{$product->id}}" type="hidden" name="id" value="{{ $product->id }}">
                                                <br>
                                                <button data-id="{{$product->id}}" class="btn btn-success saveCustomPrice" style="margin-top: 2px;"><i class="fa fa-save"></i></button>
                                                <button data-id="{{$product->id}}" class="btn btn-danger deleteCustomPrice" style="margin-top: 2px;"><i class="fa fa-trash"></i></button>
                                                <p style="color:green; margin-top:2px" id="successText-{{$product->id}}"></p>
                                                <p style="color:red; margin-top:2px" id="errorText-{{$product->id}}"></p>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $products->links() }}
                        </div>
                    </div>
                </div>
            </div><!--end #content -->
        </div><!--end #wrapper -->
    </section>
@endsection

@section('script')

<script>
$(function(){
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });
    $('.saveCustomPrice').click(function(){
        var id = $(this).attr('data-id');
        var productPrice = $('#customPrice-'+id).val();
        if(productPrice == null || productPrice == ''){
            $('#successText-'+id).html('');
            $('#errorText-'+id).html('Input a valid price!!');
            return false;
        }
        var productId = $('#customPriceId-'+id).val();
        $.ajax({
            type:'POST',
            url: window.location.origin+'/vendor/custom-price',
            data:{id : productId, price : productPrice},
            success:function(data){
                $('#successText-'+id).html('Saved!!');
                $('#errorText-'+id).html('');
            }
        });
    });
    $('.deleteCustomPrice').click(function(){
        var id = $(this).attr('data-id');
        var productPrice = $('#customPrice-'+id).val();
        if(productPrice == null || productPrice == ''){
            $('#successText-'+id).html('');
            $('#errorText-'+id).html('Nothing to delete');
            return false;
        }
        var productId = $('#customPriceId-'+id).val();
        $.ajax({
            type:'POST',
            url: window.location.origin+'/vendor/delete-custom-price',
            data:{id : productId},
            success:function(data){
                $('#customPrice-'+id).val('')
                $('#errorText-'+id).html('');
                $('#successText-'+id).html(data);
            }
        });
    });
});
</script>
@endsection
