@extends('layouts.vendor')
@section('style')
<style>
    body.homepage #content, body.how-to-top-up #content{
        background-color: unset
    }
    body.homepage #wrapper{
        background-color: unset;
        background-image: unset;
    }
    body.homepage header, body.how-to-top-up header{
        background-color: rgba(2,122,195,1);
    }
    .table p{
        display: inline;
        padding: 10px;
    }
    .tablInRow{
        background-color: #6676bb;
        width: 200px;
        min-width: 10px;
        text-align: center;
    }
</style>
@endsection
@section('content')
<section id="topSection">
        <div id="wrapper" class="clearfix">
            @include('partials._submenu')
            <div id="content">
                <div  class="container-fluid margin_vertical20">
                    <div class="margin_top50">
                        <div class="col-md-12">
                            @include('partials._message')
                            <table cellpadding="10" class="table table-bordered">
                                <h1 style="text-align: center"> All Transactions </h1 style="text-align: center"><br><br>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Sender</th>
                                        <th>Country</th>
                                        <th>Product</th>
                                        <th>Price</th>
                                        <th>Method</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>Status Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($transactions as $key => $trn)
                                        @php
                                            $topup = json_decode($trn->topup);
                                            $recharge = unserialize($trn->recharge);  
                                        @endphp
                                        <tr>
                                            <th style="vertical-align: middle">{{$key + 1}}</th>
                                            <td>{{@$trn->user->fname }} {{@$trn->user->lname }}</td>
                                            <td>{{@$recharge['selCountry']}}</td>
                                            <td>{{@$recharge['selCurrency']}}{{@$recharge['selProduct']}}</td>
                                            <td>${{@$recharge['selProductPrice']}}</td>
                                            <td>{{@$trn->method }}</td>
                                            <td>{{@$trn->created_at }}</td>
                                            <td>{{@$topup->transaction_state == 'error' ? 'Failed' : @$topup->transaction_state }}</td>
                                            <td>{{@$topup->reason}}{{@$topup->error}}{{@$topup->response->error_txt}}</td>
                                        </tr>
                                        {{-- <tr style="background-color: #fcf8e3;">
                                            <td class="tablInRow"><strong>Billing</strong> </td>
                                            <td colspan="3">
                                                <p><strong>Name:</strong>{{$billing['fname']}} {{$billing['lname']}}</p>
                                                <p><strong>Email:</strong>{{$billing['email']}}</p>
                                                <p><strong>Phone:</strong>{{$billing['phone']}}</p>
                                                <p><strong>Address:</strong>{{$billing['address']}}</p>
                                                <p><strong>City:</strong>{{$billing['city']}}</p>
                                                <p><strong>Zip:</strong>{{$billing['zip']}}</p>
                                                <p><strong>Country:</strong>{{$billing['country']}}</p>
                                            </td>
                                        </tr> --}}
                                        {{-- <tr style="background-color: #d9edf7">
                                            <td class="tablInRow"><strong>Recharge</strong></td>
                                            <td colspan="3">
                                                    <p><strong>Country:</strong>{{$recharge['selCountry']}}</p>
                                                    <p><strong>Operator:</strong>{{$recharge['selOperator']}}</p>
                                                    <p><strong>Phone:</strong>{{$recharge['selPhone']}}</p>
                                                    <p><strong>Product:</strong>{{$recharge['selProduct']}}</p>
                                                    <p><strong>Product Price:</strong>{{$recharge['selProductPrice']}}</p>
                                                    <p><strong>Product Id:</strong>{{$recharge['selProductId']}}</p>
                                            </td>
                                        </tr> --}}
                                        {{-- <tr style="background-color: #dff0d8">
                                            <td class="tablInRow"><strong>Topup</strong></td>
                                            <td colspan="3">
                                                <p><strong>Login:</strong>{{$topup->login}}</p>
                                                <p><strong>transaction_state:</strong>{{$topup->transaction_state}}</p>
                                                <p><strong>error:</strong>{{$topup->reason}}</p>
                                            </td>
                                        </tr> --}}
                                        {{-- <tr><td colspan="5"></td></tr> --}}
                                    @endforeach
                                </tbody>
                            </table>
                            {{-- {{ $transactions->links() }} --}}
                        </div>
                    </div>
                </div>
            </div><!--end #content -->
        </div><!--end #wrapper -->
    </section>
@endsection

@section('script')
<script>

</script>
@endsection
