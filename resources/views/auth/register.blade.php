@extends('layouts.login')

@section('style')
<style>
    body.homepage #content, body.how-to-top-up #content{
        background-color: unset
    }
    body.homepage #wrapper{
        background-color: unset;
        background-image: unset;
    }
    body.homepage header, body.how-to-top-up header{
        background-color: rgba(2,122,195,1);
    }
</style>
@endsection

@section('content')
<section id="topSection">
        <div id="wrapper" class="clearfix">
                    <div id="content">
                <div class="container-fluid">
        <div class="row-fluid">
            <h1 class="margin_top20">Create new account</h1>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="border_all mobile_border_none mobile_border_box_none box_shadow padding40 mobile_padding0 margin_vertical20 clearfix overflow_hidden">
                <div class="width500px marginauto mobile_border_box_none border_box">
                    @include('partials._message')
                    <form id="form_register_new_account" method="POST" action="{{ route('register') }}">
                        @csrf
                        <input type="text" name="fname" placeholder="First Name" value="{{ old('fname') }}" maxlength="50">
                        <input type="text" name="lname" placeholder="Last Name" value="{{ old('lname') }}" maxlength="50">
                        <input type="email" name="email" placeholder="Email" value="{{ old('email') }}" maxlength="50">
                        <input type="password" name="password" placeholder="Password">
                        <input type="password" name="password_confirmation" placeholder="Confirm password">

                        <input type="tel" id="i_phone" name="phone" placeholder="Phone number" maxlength="50">
                        <input type="text" id="i_address" name="address" placeholder="Address" maxlength="200">
                        <div class="width49 pull-left">
                            <input type="text" id="i_city" name="city" placeholder="City" class="half_width" maxlength="50">
                        </div>
                        <div class="width49 margin_left2 pull-left">
                            <input id="i_zip" type="text" name="zip" placeholder="Zip Code" class="half_width_margin" maxlength="50">
                        </div>
                        <div class="width100 pull-left clear">
                            <select name="country" id="i_country" class="half_width width100">
                                <option value="" selected="selected">Select country</option>
                                @foreach($data['cous'] as $c)
                                    <option value="{{$c->iso2}}">{{$c->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="submit_holder pagination-centered">
                            <button type="submit" class="btn btn-primary">Create new account</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div><!--end #content -->
</div><!--end #wrapper -->
</section>
@endsection