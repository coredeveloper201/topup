@extends('layouts.page')

@section('style')
<style>
    body.homepage #content, body.how-to-top-up #content{
        background-color: unset
    }
    body.homepage #wrapper{
        background-color: unset;
        background-image: unset;
    }
    body.homepage header, body.how-to-top-up header{
        background-color: rgba(2,122,195,1);
    }
    .hidden {
        display: none;
    }
    .pocketWrapper{
        text-align: center;
        margin-top: 100px;
    }
</style>
@endsection
@section('content')
<section id="topSection">
    <div id="wrapper" class="clearfix">
        @include('partials._submenu')
        <div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <h1 class="margin_top20">Checkout</h1>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div id="progress_bar_holder" class="margin_vertical20">
            <div class="progress_bar">
                <div class="actual_progress step5"></div>
            </div>
            <div class="clearfix">
                <div class="step pull-left active"></div><div class="step pull-right active"></div>
            </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="border_all box_shadow margin_bottom30 padding40 mobile_padding10 clearfix overflow_hidden">
                    <div class="row-fluid">
                        <div class="span12">
                            <h2 class="margin_bottom15">Order Details</h2>
                            @php
                                $topup = json_decode($trn->topup);
                                $billing = unserialize($trn->billing);
                                $recharge = unserialize($trn->recharge);
                            @endphp
                            <h4 style="background-color: coral; color:white;padding:10px">
                                {!! @$topup->reason? "@$topup->reason. Your payment has been added to your <a href='/user/pocket'>Pocket</a>":null !!}
                                {!! @$topup->error? "@$topup->error. Your payment has been added to your <a href='/user/pocket'>Pocket</a>":null !!}
                                {{ @$topup->transaction_state == 'success' ? 'Transaction successful' : null }}
                            </h4>
                            <table class="table table-products margin_bottom30 checkout_products border_all">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th class="width80px mobile_pagination_center">Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="border_top">
                                        <td>
                                            <strong>Mobile Recharge</strong>
                                        </td>
                                        <td>
                                            <span dir="ltr">
                                                <strong id="ammToSend">${{ $base['productPriceField'] }}</strong>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top:5px; padding-bottom:5px;">
                                            <span class="padding_left20">
                                                Mobile number:
                                                <span dir="ltr" class="bold">{{ $base['phoneCode'] }}{{$base['phone']}}</span>
                                                <span class="italic font-size12"> - Please make sure this number is correct.</span>
                                            </span>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top:5px; padding-bottom:5px;">
                                            <span class="padding_left20">Product:
                                                <strong id="selAmmount">{{ $base['productCurrency'] }} {{ $base['productField'] }}</strong>
                                            </span>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top:5px; padding-bottom:5px;">
                                            Operator:<span class="padding_left20" id="selOperator">{{$operator->name}}</span>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr class="border_top">
                                        <td><span class="pull-right line_height40">Total</span></td>
                                        <td class="vertical_middle padding_vertical10"><span dir="ltr"><strong id="grndTot">${{ $base['productPriceField'] }}</strong></span></td>
                                    </tr>
                                    <tr class="border_top">
                                        <td><span class="pull-right line_height40">Pocket</span></td>
                                        <td class="vertical_middle padding_vertical10"><span dir="ltr"><strong id="PocketAmount">${{Auth::user()->pocket->amount}}</strong></span></td>
                                    </tr>
                                    <tr class="border_top" style="display: none">
                                        <td><span class="pull-right line_height40">Converted Price</span></td>
                                        <td class="vertical_middle padding_vertical10"><span dir="ltr">NGN: <strong id="ngnConverted"></strong></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>                        
                </div>
            </div>
        </div>
        </div><!--end #content -->
    </div><!--end #wrapper -->
</section>
@endsection
