@extends('layouts.page')

@section('style')
<style>
    body.homepage #content, body.how-to-top-up #content{
        background-color: unset
    }
    body.homepage #wrapper{
        background-color: unset;
        background-image: unset;
    }
    body.homepage header, body.how-to-top-up header{
        background-color: rgba(2,122,195,1);
    }
</style>
@endsection
@section('content')
<section id="topSection">
        <div id="wrapper" class="clearfix">
            @include('partials._submenu')
            <div id="content">
                <div class="container margin_bottom20 margin_top0">
                    <div class="margin_top20 visible-phone"><select class="phone_account_nav visible-phone width100 margin_bottom20">
                            <option value="account/home">Dashboard</option>
                                <option value="account/information">My Information</option>
                        </select>
                    </div>
                    {{-- <div class="accordion styled_acc row-fluid margin_top20" id="account_settings_acc">
                        {!! $account ? 
                            '<div class="alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h3>Vendor Domain</h3>
                                Domain Listed
                            </div>
                            ' : '
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h3>Vendor Domain</h3>
                                This Domain Is Not Listed
                            </div>' 
                        !!}
                    </div> --}}
                    <div class="accordion styled_acc row-fluid margin_top20" id="account_settings_acc">
                        <div class="accordion-group">
                            <div class="accordion-heading position_relative">
                                <a class="accordion-toggle styled_acc inactive open" data-toggle="collapse" href="#_account_settings_mobile_recharge">
                                        <span class="mobile_float_left bigicon-mr display_table_cell"></span>
                                        <span class="hidden-small-phone mobile_float_left mobile_padding_top15 display_table_cell valign_middle padding_left10 line_height1">Top Up</span>
                                    <div class="mobile_float_right tabs_more_info font-size18 line_height40">
                                        <span class="hidden-phone"></span>
                                        <div class="margin_left20 arrow_accordion_heading valign_middle"></div>
                                    </div>
                                </a>
                            </div>
                            <div id="_account_settings_mobile_recharge" class="accordion-body collapse in">
                                <div class="accordion-inner">
                                    <p>You may call it top up, mobile airtime, mobile credit, mobile load or whatever you want. You're in the right place to send credit to a mobile anywhere in the world.</p>
                                    <div>
                                        <p>Easy, fast, and reliable!</p>
                                        <a href="{{route('rechargeNow')}}" class="btn btn-primary">Recharge Now »</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    </div>

            </div><!--end #content -->
        </div><!--end #wrapper -->
    </section>
@endsection
