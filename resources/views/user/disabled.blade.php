@extends('layouts.page')

@section('style')
<style>
    body.homepage #content, body.how-to-top-up #content{
        background-color: unset
    }
    body.homepage #wrapper{
        background-color: unset;
        background-image: unset;
    }
    body.homepage header, body.how-to-top-up header{
        background-color: rgba(2,122,195,1);
    }
</style>
@endsection
@section('content')
<section id="topSection">
        <div id="wrapper" class="clearfix">
            @include('partials._submenu')
            <div id="content">
                <div  class="container-fluid margin_vertical20">
                    <div class="row-fluid margin_top20">
                        <div class="col-md-12" style="text-align: center; width:100%">
                            <img src="{{url('img/disabled.jpg')}}" alt="disabled">
                            <div>
                                <h1><strong> Your account is disabled</strong></h1>
                                <h2 style="color:#7d7c7c; margin-bottom:50px">Please contact <strong> info@topupClone.com</strong></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--end #content -->
        </div><!--end #wrapper -->
    </section>
@endsection
