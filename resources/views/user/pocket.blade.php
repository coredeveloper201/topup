@extends('layouts.page')
@section('style')
<style>
    body.homepage #content, body.how-to-top-up #content{
        background-color: unset
    }
    body.homepage #wrapper{
        background-color: unset;
        background-image: unset;
    }
    body.homepage header, body.how-to-top-up header{
        background-color: rgba(2,122,195,1);
    }
    .table p{
        display: inline;
        padding: 10px;
    }
    .tablInRow{
        background-color: #6676bb;
        width: 200px;
        min-width: 10px;
        text-align: center;
    }
</style>
@endsection
@section('content')
<section id="topSection">
        <div id="wrapper" class="clearfix">
            @include('partials._submenu')
            <div id="content">
                <div class="container-fluid margin_vertical20">
                    <div class="row-fluid margin_top50">
                        <div class="col-md-12">
                            <h3>The Pocket shows the amount of money you have on the system, if you perform a transaction and you make payment but the topup service fails then we put your payment in the pocket. Next time you are performing the same transaction or another one, we make use of the money in your pocket first.</h3>
                            <h1 style="text-align: center">
                                Pocket Balance: <br> {{ $pocket->amount }} USD
                            </h1>
                        </div>
                        <div class="col-md-12">
                            @include('partials._message')
                            <table cellpadding="10" class="table table-bordered">
                                <h3 style="text-align: center"> Transaction History </h3><br>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Method</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Status Details</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($transactions as $key => $trn)
                                        <tr>
                                            @php
                                                $topup = json_decode($trn->topup);
                                                $recharge = unserialize($trn->recharge);
                                            @endphp
                                            <th style="vertical-align: middle" scope="row">{{$key + 1}}</th>
                                            <td>{{ $trn->method }}</td>
                                            <td>${{$recharge['selProductPrice']}}</td>
                                            <td>{{@$topup->transaction_state}}</td>
                                            <td>{{@$topup->reason}}{{@$topup->error}}{{@$topup->response->error_txt}}</td>
                                            <td>{{ $trn->created_at }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{-- {{ $transactions->links() }} --}}
                        </div>
                    </div>
                </div>
            </div><!--end #content -->
        </div><!--end #wrapper -->
    </section>
@endsection

@section('script')
<script>

</script>
@endsection
