<?php

use Illuminate\Http\Request;

Route::get('get-operator-by-country/{cod}', 'PassportController@getCountryOperator')->where('cod', '[A-Za-z]+');
Route::post('/transaction-wall/{amount}/{cur}', 'PassportController@payWallResponse')->name('payWallResponse')->where('amount', '[0-9\.]+')->where('cur', '[A-Za-z]+');
