<?php

Route::get('/', 'PageController@home')->name('home');
Route::get('/how-to-top-up', 'PageController@howToTopup')->name('howToTopup');
Route::get('/privacy', 'PageController@privacyPolicy')->name('privacyPolicy');
Route::get('/terms', 'PageController@termCond')->name('termCond');
Route::get('/help-center', 'PageController@helpCenter')->name('helpCenter');
Route::get('/vendor-login', 'PageController@vendorLogin')->name('vendorLogin');
Route::get('/admin-login', 'PageController@adminLogin')->name('adminLogin');

Route::get('user/recharge-now/{id?}', 'UserController@rechargeNow')->name('rechargeNow');
Route::post('user/recharge-now', 'UserController@postRechargeNow')->name('postRechargeNow');

Route::get('/user/get-operator-by-country/{cod}', 'UserController@getCountryOperator')->where('cod', '[0-9]+');
Route::get('/user/get-products-by-operator/{id}', 'UserController@getOperatorProduct')->where('id', '[0-9]+');






Route::middleware(['auth'])->prefix('user')->group(function () {
    Route::get('/home', 'UserController@userHome')->name('userHome');
    Route::get('/information', 'UserController@userInformation')->name('userInformation');
    Route::post('/post', 'UserController@userChangeProfile')->name('userChangeProfile');

    Route::get('/transaction', 'UserController@userTransaction')->name('userTransaction');
    Route::get('/pocket', 'UserController@userPocket')->name('userPocket');
    
    Route::post('/change-email', 'UserController@userChangeEmail')->name('userChangeEmail');
    Route::post('/change-password', 'UserController@userChangePassword')->name('userChangePassword');
    Route::post('/change-timezone', 'UserController@userChangeTimezone')->name('userChangeTimezone');
    
    
    Route::get('/go-to-checkout', 'UserController@goToCheckout')->name('goToCheckout');
    Route::get('confirm-order/{id}', 'UserController@confirmOrder')->name('confirmOrder')->where('id', '[0-9]+');

    Route::post('/successfull-transaction', 'TransactionController@successTransaction')->name('successTransaction');
    Route::post('/successfull-transaction-paystack', 'TransactionController@transPaystack')->name('transPaystack');
    Route::post('/successfull-transaction-voguepay', 'TransactionController@transVoguepay')->name('transVoguepay');
    Route::post('/successfull-transaction-wall', 'TransactionController@payWallResponse')->name('payWallResponse');
    Route::post('/successfull-transaction-paymentwall', 'TransactionController@transPayWall')->name('transPayWall');
    Route::post('/pocket-transaction', 'TransactionController@pocketTrans')->name('pocketTrans');
});

Route::get('user/account-disabled', 'UserController@userAccountDisabled')->name('userAccountDisabled')->middleware('auth');

Route::middleware(['vendor'])->prefix('vendor')->group(function () {
    Route::get('/home', 'VendorController@vendorHome')->name('vendorHome');
    Route::post('/recharge-now', 'VendorController@postRechargeNowVendor')->name('postRechargeNowVendor');
    Route::get('/users', 'VendorController@vendorUsers')->name('vendorUsers')->where('id', '[0-9]+');
    Route::get('/users/{id}', 'VendorController@vendorUser')->name('vendorUser')->where('id', '[0-9]+');
    Route::get('/users/disable/{id}', 'VendorController@userChangeStat')->name('userChangeStat')->where('id', '[0-9]+');

    Route::get('activate-method/{id}', 'VendorController@vendorActivateMethod')->name('vendorActivateMethod')->where('id', '[0-9]+');

    Route::post('activate-rave', 'VendorController@userApiRave')->name('userApiRave');
    Route::post('activate-wall', 'VendorController@userApiWall')->name('userApiWall');
    Route::post('activate-stack', 'VendorController@userApiStack')->name('userApiStack');
    Route::post('activate-Vogue', 'VendorController@userApiVogue')->name('userApiVogue');

    Route::get('credentials', 'VendorController@vendorCredentials')->name('vendorCredentials');
    Route::post('credentials', 'VendorController@vendorPostApiCred')->name('vendorPostApiCred');
    Route::post('social-links', 'VendorController@vendorPostSocial')->name('vendorPostSocial');

    Route::get('set-price/{country?}/{operator?}', 'VendorController@vendorPricing')->name('vendorPricing')->where('country', '[0-9]+')->where('operator', '[0-9]+');

    Route::post('custom-price', 'VendorController@vendorSetCustomPrice')->name('vendorSetCustomPrice');
    Route::post('delete-custom-price', 'VendorController@vendorDeleteCustomPrice')->name('vendorDeleteCustomPrice');

    Route::get('transaction', 'VendorController@vendorTransactions')->name('vendorTransactions');

    Route::post('profile-image', 'VendorController@vendorLogoChange')->name('vendorLogoChange');
    Route::get('/information', 'VendorController@vendorInformation')->name('vendorInformation');
});
Route::middleware(['admin'])->prefix('admin')->group(function () {
    Route::get('/home', 'AdminController@adminHome')->name('adminHome');
    Route::get('create-vendor', 'AdminController@adminCreateVendor')->name('adminCreateVendor');
    Route::post('create-vendor', 'AdminController@postCreateVendor')->name('postCreateVendor');

    Route::get('vendor-edit/{id}', 'AdminController@adminVendorEdit')->name('adminVendorEdit')->where('id', '[0-9]+');
    Route::post('vendor-edit/{id}', 'AdminController@postUpdateVendor')->name('postUpdateVendor')->where('id', '[0-9]+');
    Route::get('/information', 'AdminController@adminInformation')->name('adminInformation');

});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


    // Reset Databases
    Route::get('/insertCountry', 'VendorController@insertCountry');
    Route::get('/mergeCountry', 'VendorController@mergeCountry');
    Route::get('/insertOperator', 'VendorController@insertOperator');
    Route::get('/insertProduct', 'VendorController@insertProduct');