<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\User;
use App\Country;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $data = array();

        $cons = Country::all();
        $data['cous'] = $cons;

        $this->domain = url('/');
        $this->vendor = User::select('id', 'domain', 'logo','logotext')->where('domain', url('/'))->first();
        // Sharing is caring
        // dd($this->vendor);
        if($this->vendor){
            if($this->vendor->logotext){
                
                $data['logo'] = $this->vendor->logotext;
                view()->share('data', $data);
            }else{
                $data['logo'] = 'TopUp';
                view()->share('data', $data);
            }
        }else{
            $data['logo'] = 'TopUp';
            view()->share('data', $data);
        }
    }
}
