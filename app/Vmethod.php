<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vmethod extends Model
{
    protected $fillable = [
        'name', 'user_id'
    ];
}
