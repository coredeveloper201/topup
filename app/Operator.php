<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operator extends Model
{
    protected $fillable = [
        'name', 'country_id', 'country_name'
    ];

    public function products(){
        return $this->hasMany('App\Product');
    }
    public function country(){
        return $this->belongsTo('App\Country');
    }
}
