<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $role = Auth::user()->role;
            // dd($role);
            if(in_array('admin', explode(',', Auth::user()->role))){
                return redirect('/admin/home');
            }elseif(in_array('vendor', explode(',', Auth::user()->role))){
                return redirect('/vendor/home');
            }elseif(in_array('user', explode(',', Auth::user()->role))){
                return redirect('/user/home');
            }else{
                return redirect('/');
            }
        }

        return $next($request);
    }
}
