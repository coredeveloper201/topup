<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';
    protected $username;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->emailaddress = $this->modifyEmail();
    }
    public function modifyEmail(){
        $login = request()->input('login');
        $email = request()->input('email');

        $vendor = User::select('id', 'domain')->where('domain', url('/'))->first();
        $user = User::select('role')->where('email', $vendor->id.'-'.$email)->first();
        if($user){
            if($user->role === 'user'){
                request()->merge(['email' => $vendor->id.'-'.$email]);
            }
        }
        return true;
    }

    public function redirectTo(){
        // User role
        $role = Auth::user()->role;
        if(in_array('admin', explode(',', Auth::user()->role))){
            return '/admin/home';
        }elseif(in_array('vendor', explode(',', Auth::user()->role))){
            return '/vendor/home';
        }elseif(in_array('user', explode(',', Auth::user()->role))){
            return '/user/home';
        }else{
            return '/';
        }
    }
}
