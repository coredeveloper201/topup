<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Profile;
use App\Pocket;
use App\Country;
use Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone'   =>  ['required','string'],
            'address'   =>['required','string'],
            'city'   =>['required','string'],
            'zip'   =>['required','integer'],
            'country'   =>['required','string'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $this->redirectTo = '/user/home';
        $vendor = User::select('id', 'domain')->where('domain', url('/'))->first();
        if($vendor){
            $userEmail = User::where('email', $vendor->id.'-'.$data['email'])->where('vendor_id', $vendor->id)->first();

            if(!$userEmail){
                $user = User::create([
                    'fname' => $data['fname'],
                    'lname' => $data['lname'],
                    'email' => $vendor->id.'-'.$data['email'],
                    'password' => Hash::make($data['password']),
                    'phone' => $data['phone'],
                    'role' => 'user',
                    'vendor_id' => $vendor->id,
                ]);
                $profile = new Profile;
                $profile->user_id = $user->id;
                $profile->address = $data['address'];
                $profile->city = $data['city'];
                $profile->zip = $data['zip'];
                $profile->country = $data['country'];
                $profile->save();

                $pocket = new Pocket;
                $pocket->user_id = $user->id;
                $pocket->vendor_id = $vendor->id;
                $pocket->save();
        
                return $user;
            }
            dd('User Already Exist');
        }
        dd('Vendor Not Registered!');
    }
}
