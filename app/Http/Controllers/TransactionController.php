<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Country;
use App\Operator;
use App\Product;
use App\Transaction;
use App\Vmethod;
use App\Pocket;
use App\Setup;
use Hash;
use Auth;
use Session;


class TransactionController extends Controller
{
    public function transPayWall(Request $request){
        $vendor = $this->vendor;
        $topup = $this->vendor->setting;


        $trans = Transaction::find($request->dataAs - 45821548456);
        if(!$trans){
            return response()->json('Something Went Wrong');
        }
        if($trans->response != 'PreTrans'){
            return response()->json('Something Went Wrong');
        }

        $trans->user_id = Auth::id();
        $trans->vendor_id = $vendor->id;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://bosdia.com/TopUp/api/top_up",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "api_user_name=".$topup->apiUser."&api_password=".$topup->apiPassword."&destination_msisdn=".$request->selPhone."&msisdn=".$request->email."&product=".$request->selProduct,
            CURLOPT_HTTPHEADER => array(
                "Content-Type"=> "application/json",
                "X-Requested-With"=> "XMLHttpRequest",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            $trans->topup = $err;
        } else {
            $trans->topup = $response;
        }

        $billing = $this->billingArray($request);
        $recharge = $this->rechargeArray($request);

        $trans->billing = serialize($billing);
        $trans->recharge = serialize($recharge);
        $trans->response = $request->txref;
        $trans->method = 'Payment Wall';
        $trans->fullPocket = 0;

        
        $pocket = Pocket::where('vendor_id', $vendor->id)->where('user_id', Auth::id())->first();
        
        $trans->pocketbalance = $pocket->amount;
        $trans->save();

        if($request->lessPocket){
            $pocket->amount = 0;
            $pocket->save();
        }

        if ($err) {
            $trans->topup = json_encode($err);
        } else {
            $response = json_decode($response);
            if($response->transaction_state != 'success'){
                if($response->transaction_state == 'error' || $response->status == 'error' || $response->login == 'failed'){
                    $insertPocket = $this->inserToPocket($vendor->id, Auth::id(), $request->selProductPrice);
                }
            }
        }

        $frequency = Vmethod::where('user_id', $vendor->id)->where('name', 'Paymentwall')->first();
        if($frequency->frequency > 0){
            $frequency->frequency -= 1;
            $frequency->save();
        }

        $methods = Vmethod::where('user_id', $vendor->id)->where('active', 1)->orderby('queue', 'asc')->get();
        if($methods->last()->frequency <= 0){
            if($methods->last()->name = 'Paymentwall'){
                $this->cycleFrequency();
            }
        }

        return response()->json($trans);
    }
    public function transVoguepay(Request $request){
        $vendor = $this->vendor;
        $topup = $this->vendor->setting;
        $trans = new Transaction;
        $trans->user_id = Auth::id();
        $trans->vendor_id = $vendor->id;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://bosdia.com/TopUp/api/top_up",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "api_user_name=".$topup->apiUser."&api_password=".$topup->apiPassword."&destination_msisdn=".$request->selPhone."&msisdn=".$request->email."&product=".$request->selProduct,
            CURLOPT_HTTPHEADER => array(
                "Content-Type"=> "application/json",
                "X-Requested-With"=> "XMLHttpRequest",
            ),
        ));
 
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $trans->topup = $err;
        } else {
            $trans->topup = $response;
        }

        $billing = $this->billingArray($request);
        $recharge = $this->rechargeArray($request);

        $trans->billing = serialize($billing);
        $trans->recharge = serialize($recharge);
        $trans->response = $request->txref;
        $trans->method = 'Voguepay';
        $trans->fullPocket = 0;

        
        $pocket = Pocket::where('vendor_id', $vendor->id)->where('user_id', Auth::id())->first();
        $trans->pocketbalance = $pocket->amount;
        $trans->save();

        if($request->lessPocket){
            $pocket->amount = 0;
            $pocket->save();
        }
        
        

        if ($err) {
            $trans->topup = json_encode($err);
        } else {
            $response = json_decode($response);
            if($response->transaction_state != 'success'){
                if($response->transaction_state == 'error' || $response->status == 'error' || $response->login == 'failed'){
                    $insertPocket = $this->inserToPocket($vendor->id, Auth::id(), $request->selProductPrice);
                }
            }
        }

        $frequency = Vmethod::where('user_id', $vendor->id)->where('name', 'Voguepay')->first();
        if($frequency->frequency > 0){
            $frequency->frequency -= 1;
            $frequency->save();
        }

        $methods = Vmethod::where('user_id', $vendor->id)->where('active', 1)->orderby('queue', 'asc')->get();
        if($methods->last()->frequency <= 0){
            if($methods->last()->name = 'Voguepay'){
                $this->cycleFrequency();
            }
        }

        return response()->json($trans);
    }
    public function pocketTrans(Request $request){
        $vendor = $this->vendor;
        $topup = $this->vendor->setting;
        $trans = new Transaction;
        $trans->user_id = Auth::id();
        $trans->vendor_id = $vendor->id;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://bosdia.com/TopUp/api/top_up",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "api_user_name=".$topup->apiUser."&api_password=".$topup->apiPassword."&destination_msisdn=".$request->selPhone."&msisdn=".$request->email."&product=".$request->selProduct,
            CURLOPT_HTTPHEADER => array(
                "Content-Type"=> "application/json",
                "X-Requested-With"=> "XMLHttpRequest",
            ),
        ));
 
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $trans->topup = $err;
        } else {
            $trans->topup = $response;
        }

        $billing = $this->billingArray($request);
        $recharge = $this->rechargeArray($request);
        
        $trans->billing = serialize($billing);
        $trans->recharge = serialize($recharge);
        $trans->response = 'Full Pocket Transaction';
        $trans->method = 'Full Pocket Transaction';
        $trans->pocket = $request->selProductPrice;
        $trans->fullPocket = 1;

        $pocket = Pocket::where('vendor_id', $vendor->id)->where('user_id', Auth::id())->first();
        $trans->pocketbalance = $pocket->amount;
        $trans->save();

        $removePocket = $this->removeFromPocket($vendor->id, Auth::id(), $request->selProductPrice);
        

        if ($err) {
            $trans->topup = json_encode($err);
        } else {
            $response = json_decode($response);
            if($response->transaction_state != 'success'){
                if($response->transaction_state == 'error' || $response->status == 'error' || $response->login == 'failed'){
                    $insertPocket = $this->inserToPocket($vendor->id, Auth::id(), $request->selProductPrice);
                }
            }
        }

        return response()->json($trans);
    }
    public function transPaystack(Request $request){

        $accounts = $this->vendor->setup;
        $stack = unserialize($accounts->paystack);

        $txref = Transaction::where('response', $request->txref)->where('method', 'Paystack')->first();
        if($txref){
            return response()->json('Transaction already Sent');
        }

        $paystack = new \Yabacon\Paystack($stack['secretKey']);
        try
        {
            $tranx = $paystack->transaction->verify([
                'reference'=>$request->txref,
            ]);
        } catch(\Yabacon\Paystack\Exception\ApiException $e){
            print_r($e->getResponseObject());
            die($e->getMessage());
        }

        if ('success' !== $tranx->data->status) {
            return response()->json('Something went wrong');
        }


        $vendor = $this->vendor;
        $topup = $this->vendor->setting;
        $trans = new Transaction;
        $trans->user_id = Auth::id();
        $trans->vendor_id = $vendor->id;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://bosdia.com/TopUp/api/top_up",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "api_user_name=".$topup->apiUser."&api_password=".$topup->apiPassword."&destination_msisdn=".$request->selPhone."&msisdn=".$request->email."&product=".$request->selProduct,
            CURLOPT_HTTPHEADER => array(
                "Content-Type"=> "application/json",
                "X-Requested-With"=> "XMLHttpRequest",
            ),
        ));
 
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $trans->topup = $err;
        } else {
            $trans->topup = $response;
        }

        $billing = $this->billingArray($request);
        $recharge = $this->rechargeArray($request);

        $trans->billing = serialize($billing);
        $trans->recharge = serialize($recharge);
        $trans->response = $request->txref;
        $trans->method = 'Paystack';
        $trans->fullPocket = 1;
        
        
        $pocket = Pocket::where('vendor_id', $vendor->id)->where('user_id', Auth::id())->first();
        $trans->pocketbalance = $pocket->amount;
        $trans->save();

        if($request->lessPocket){
            $pocket->amount = 0;
            $pocket->save();
        }
        
        

        if ($err) {
            $trans->topup = json_encode($err);
        } else {
            $response = json_decode($response);
            if($response->transaction_state != 'success'){
                if($response->transaction_state == 'error' || $response->status == 'error' || $response->login == 'failed'){
                    $insertPocket = $this->inserToPocket($vendor->id, Auth::id(), $request->selProductPrice);
                }
            }
        }

        $frequency = Vmethod::where('user_id', $vendor->id)->where('name', 'Paystack')->first();
        if($frequency->frequency > 0){
            $frequency->frequency -= 1;
            $frequency->save();
        }

        $methods = Vmethod::where('user_id', $vendor->id)->where('active', 1)->orderby('queue', 'asc')->get();
        if($methods->last()->frequency <= 0){
            if($methods->last()->name = 'Paystack'){
                $this->cycleFrequency();
            }
        }

        return response()->json($trans);
    }
    public function successTransaction(Request $request){

        $accounts = $this->vendor->setup;
        $rave = unserialize($accounts->rave);

        $txref = Transaction::where('response', $request->txref)->where('method', 'Rave')->first();
        if($txref){
            return response()->json('Transaction already Sent');
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.ravepay.co/flwv3-pug/getpaidx/api/v2/verify",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "txref=".$request->txref."&SECKEY=".$rave['secretKey'],
            CURLOPT_HTTPHEADER => array(
                "Content-Type"=> "application/json",
            ),
        ));
        $response = curl_exec($curl);

        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return response()->json('Something went wrong!!');
        } else {
            $res = json_decode($response);
            if($res->status != 'success'){
                return response()->json('Something went wrong!!');
            }
        }


        $vendor = $this->vendor;
        $topup = $this->vendor->setting;
        $trans = new Transaction;
        $trans->user_id = Auth::id();
        $trans->vendor_id = $vendor->id;

        // return $request->selPhone;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://bosdia.com/TopUp/api/top_up",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "api_user_name=".$topup->apiUser."&api_password=".$topup->apiPassword."&destination_msisdn=".$request->selPhone."&msisdn=".$request->email."&product=".$request->selProduct,
            CURLOPT_HTTPHEADER => array(
                "Content-Type"=> "application/json",
                "X-Requested-With"=> "XMLHttpRequest",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $trans->topup = $err;
        } else {
            $trans->topup = $response;
        }

        $billing = $this->billingArray($request);
        $recharge = $this->rechargeArray($request);
        
        $trans->billing = serialize($billing);
        $trans->recharge = serialize($recharge);
        $trans->response = $request->txref;
        $trans->method = 'Rave';
        $trans->fullPocket = 0;
        
        $pocket = Pocket::where('vendor_id', $vendor->id)->where('user_id', Auth::id())->first();
        $trans->pocketbalance = $pocket->amount;
        $trans->save();
        
        if($request->lessPocket){
            $pocket->amount = 0;
            $pocket->save();
        }
        
        

        if ($err) {
            $trans->topup = json_encode($err);
        } else {
            $response = json_decode($response);
            if($response->transaction_state != 'success'){
                if($response->transaction_state == 'error' || $response->status == 'error' || $response->login == 'failed'){
                    $insertPocket = $this->inserToPocket($vendor->id, Auth::id(), $request->selProductPrice);
                }
            }
        }

        $frequency = Vmethod::where('user_id', $vendor->id)->where('name', 'Rave')->first();
        if($frequency->frequency > 0){
            $frequency->frequency -= 1;
            $frequency->save();
        }

        $methods = Vmethod::where('user_id', $vendor->id)->where('active', 1)->orderby('queue', 'asc')->get();
        if($methods->last()->frequency <= 0){
            if($methods->last()->name = 'Rave'){
                $this->cycleFrequency();
            }
        }

        return response()->json($trans);
    }
    public function payWallResponse($amount,$cur){
        require_once 'paymentwall/lib/paymentwall.php';
        Paymentwall_Config::getInstance()->set(array(
            'private_key' => 't_32285e17e3ea72f648e61132aebb9c'
        ));

        $cardInfo = array(
            'email' => $_POST['email'],
            'amount' => $amount,
            'currency' => $cur,
            'token' => $_POST['brick_token'],
            'fingerprint' => $_POST['brick_fingerprint'],
            'description' => 'Order #123'
        );
        $charge = new Paymentwall_Charge();
        $charge->create($cardInfo);
        $response = $charge->getPublicData();
        if ($charge->isSuccessful()) {
            if ($charge->isCaptured()) {
                // deliver s product
            } elseif ($charge->isUnderReview()) {
                // decide on risk charge
            }
        } else {
            $errors = json_decode($response, true);
        }
        echo $response;
    }










    private function cycleFrequency(){
        $vendor = $this->vendor;
        $methods = Vmethod::where('user_id', $vendor->id)->where('active', 1)->get();
        foreach($methods as $m){
            $setup = Setup::where('user_id', $vendor->id)->first();
            if($m->name == 'Rave'){
                $rave = unserialize($vendor->setup->rave);
                $m->frequency = $rave['frequency'];
                $m->save();
            }
            if($m->name == 'Paymentwall'){
                $wall = unserialize($vendor->setup->paymentwall);
                $m->frequency = $wall['frequency'];
                $m->save();
            }
            if($m->name == 'Paystack'){
                $stack = unserialize($vendor->setup->paystack);
                $m->frequency = $stack['frequency'];
                $m->save();
            }
            if($m->name == 'Voguepay'){
                $pay = unserialize($vendor->setup->voguepay);
                $m->frequency = $pay['frequency'];
                $m->save();
            }
        }
    }
    private function inserToPocket($vId, $uId, $ammount){
        $pocket = Pocket::where('vendor_id', $vId)->where('user_id', $uId)->first();
        if($pocket){
            $pocket->amount += $ammount;
            $pocket->save();
            return $pocket;
        }else{
            $pocket = new Pocket;
            $pocket->user_id = $uId;
            $pocket->vendor_id = $vId;
            $pocket->amount = $ammount;
            $pocket->save();
            return $pocket;
        }
    }
    private function removeFromPocket($vId, $uId, $ammount){
        $pocket = Pocket::where('vendor_id', $vId)->where('user_id', $uId)->first();
        if($pocket){
            $pocket->amount -= $ammount;
            $pocket->save();
            return $pocket;
        }
    }
    private function billingArray($request){
        $billing = [
            'fname' => $request->fname,
            'lname' => $request->lname,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'city' => $request->city,
            'zip' => $request->zip,
            'country' => $request->country,
        ];
        return $billing;
    }
    private function rechargeArray($request){
        $country = Country::find($request->selCountry);
        $product = Product::find($request->selProductId);
        $recharge = [
            'selCountryId' => $request->selCountry,
            'selCountry' => $country->name,
            'selCurrency' => $product->pcur,
            'selOperator' => $request->selOperator,
            'selPhone' => $request->selPhone,
            'selProduct' => $request->selProduct,
            'selProductPrice' => $request->selProductPrice,
            'selProductPriceCurrency' => $request->selProductPriceCurrency,
            'selProductId' => $request->selProductId,
        ];
        return $recharge;
    }
    public function gizzleClient(){
        // $headers = [
        //     'Content-Type' => 'application/json',
        //     'X-Requested-With' => 'XMLHttpRequest',
        //   ];
        // $client = new Client([
        //     'headers' => $headers
        // ]);
        // $response = $client->POST('https://bosdia.com/TopUp/api/top_up', [
        //     'form_data' => [
        //         'api_user_name' => "vendor1api",
        //         'api_password' => "vendor1api@1000",
        //         'destination_msisdn' => "vendor1api@1000",
        //         'msisdn' => "nasir@khan.com",
        //         'product' => "20"
        //     ]
        // ]);
        // if($response->getStatusCode() == 200){
        //     $resp = $response->getBody();
        //     $obj = json_decode($resp);
        //     dd($obj);

        //     $trans->method = $obj;
        // }else{
        //     $trans->method = json_decode($response);
        // }
    }
}
