<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use App\Transaction;
use Auth;

use Illuminate\Http\Request;
use Paymentwall_Charge;

class PassportController extends Controller
{
    public function getCountryOperator($cod){
        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/com.reloadly.topups-v1+json',
            'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UazNORGhDUXpFd1FUZ3hNVU14T0RkR1FUVTBNekEzT0RCRk1qZ3dNMEpHTTBNNVJrWkVPQSJ9.eyJodHRwczovL3JlbG9hZGx5LmNvbS9qdGkiOiI1M2RmYTA1Ni00NGVjLTQ1MTctOTg2NS04YWJlMWMyOTE2ZWEiLCJodHRwczovL3JlbG9hZGx5LmNvbS9wcmVwYWlkVXNlcklkIjoiMTA3NCIsImlzcyI6Imh0dHBzOi8vcmVsb2FkbHkuYXV0aDAuY29tLyIsInN1YiI6IjBCbVFmMUZ3d1ZXT2pyQnpUNzVTak5tT29QeGlqSWFUQGNsaWVudHMiLCJhdWQiOiJodHRwczovL3RvcHVwcy5yZWxvYWRseS5jb20iLCJpYXQiOjE1NjE5NTYwMzIsImV4cCI6MTU2NzE0MDAzMiwiYXpwIjoiMEJtUWYxRnd3VldPanJCelQ3NVNqTm1Pb1B4aWpJYVQiLCJzY29wZSI6InNlbmQtdG9wdXBzIHJlYWQtdG9wdXBzLWhpc3RvcnkgcmVhZC1vcGVyYXRvcnMgcmVhZC1wcm9tb3Rpb25zIHJlYWQtcHJlcGFpZC1iYWxhbmNlIHJlYWQtcHJlcGFpZC1jb21taXNzaW9ucyIsImd0eSI6ImNsaWVudC1jcmVkZW50aWFscyJ9.ZIjl2dz8ue0ehMBHO2FwiRkKXnEMaFzKfn0MdLnio7oneHBRapCgIVJeomnQYJ07VKE-DhkrAmLoEIN2Ihj4om8hXVzFQQUrmuf07a_F6kAI5cNy4ADpdmFjoESpuU9gLYZxsQS1IOgZ1c1tX9bWpz6bnJtAg6fQtPVYGFyE0ihBL7EqHSB80d3r7sBuWqSZFXCWAMhocHOcThh9tlxNLXfPIb7i45_448X6ERC9Ztm6b27leLA9qFzpvbVXC1APLwlwAihGq2DDwZikHKOpTzU31eBaDSZBHANiJlmMqRF0Xfs-VN_vZeRywUFh1QtclsqwedbdXozwqWndzTt9-g',
          ];
        $client = new Client([
            'headers' => $headers
        ]);
        $response = $client->GET('https://topups.reloadly.com/operators/countries/'.$cod);
        if($response->getStatusCode() == 200){
            $resp = $response->getBody();
            $obj = json_decode($resp);

            $view = view('ajax.operator',compact('obj'))->render();
            return response()->json($view);
        }
        return response()->json($response);
    }
    public function payWallResponse($amount, $cur){
        $vendor = $this->vendor;
        $accounts = $this->vendor->setup;
        $wall = unserialize($accounts->paymentwall);
        \Paymentwall_Config::getInstance()->set(array(
            'private_key' => $wall['secretKey']
        ));
                
        $cardInfo = array(
            'email' => $_POST['email'],
            'amount' => $amount,
            'currency' => $cur,
            'token' => $_POST['brick_token'],
            'fingerprint' => $_POST['brick_fingerprint'],
            'description' => 'Order #123'
        );
        $charge = new Paymentwall_Charge();
        $charge->create($cardInfo);
        $response = $charge->getPublicData();
        if ($charge->isSuccessful()) {
            if ($charge->isCaptured()) {
                $trans = new Transaction;
                $trans->user_id = 12;
                $trans->vendor_id = 12;
                $trans->billing = 'Billing';
                $trans->recharge = 'Recharge';
                $trans->response = 'PreTrans';
                $trans->method = 'Payment Wall';
                $trans->save();
            } elseif ($charge->isUnderReview()) {
                $trans = new Transaction;
                $trans->user_id = 12;
                $trans->vendor_id = 12;
                $trans->billing = 'Billing';
                $trans->recharge = 'Recharge';
                $trans->response = 'PreTrans';
                $trans->method = 'Payment Wall';
                $trans->save();
            }
        } else {
            $errors = json_decode($response, true);
        }
        $new['response'] = json_decode($response, true);
        $new['resas'] = $trans->id + 45821548456;
        $new['success'] = 1;
        $new['message'] = 'message';
        echo json_encode($new, true);
        exit();

    }
}
