<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
        'name','code',
    ];
    public function operators(){
        return $this->hasMany('App\Operator');
    }
}
