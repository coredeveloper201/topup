<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        'email', 'password', 'fname', 'lname','currency','newsletter','role', 'vendor_id', 'phone'
    ];
    protected $hidden = [
        'password','remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile(){
        return $this->hasOne('App\Profile');
    }
    public function transactions(){
        return $this->hasMany('App\Transaction', 'vendor_id');
    }
    public function setting(){
        return $this->hasOne('App\Setting');
    }
    public function setup(){
        return $this->hasOne('App\Setup');
    }
    public function methods(){
        return $this->hasMany('App\Vmethod');
    }
    public function pocket(){
        return $this->hasOne('App\Pocket');
    }
}
